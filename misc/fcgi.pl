#!/usr/bin/perl

use strict;
use utf8;

use Data::Dumper;
use FCGI;
use CGI;
use Template;
use YAML::XS;
use Encode;
use JSON;

use File::Basename;
use FindBin qw($RealBin);
# use lib dirname($RealBin);

use lib 'modules';
use Admin;
use Translate;
use Plural;

my $Template = Template->new(
    'INCLUDE_PATH' => "${RealBin}/tmpl"
) or die "Cannot create Template object: ". $Template::ERROR;

my %TemplateSubs = (
    'translate' => sub {
        my ($Language, $String) = @_;

        return Translate::Do(
            'Language' => $Language,
            'String' => $String
        );
    },
    'plural' => sub {
        my ($String) = @_;

        return Plural::Do(
            'String' => $String
        );
    },
);

my $DBAccount = {
    'host' => 'localhost',
    'name' => 'flowers',
    'user' => 'flowers',
    'pass' => '123'
};

my $Admin = Admin->new(
    'DBAccount' => {
        'main' => $DBAccount
    }
);

my @URLs = (
    {
        'Test' => sub {
            (CGI::param('a') =~ /JSON/) ? 0 : 1
        },
        'Run' => \&TopMenuSimple,
    },

    # Show available objects
    {
        'Test' => sub {
            ($ENV{'SCRIPT_NAME'} eq '/cgi-bin/fcgi.pl')
            && (CGI::param('a') eq 'list')
            && (CGI::param('o') eq 'any')
        },
        'Run' => \&PrintHTML_Objects,
        'Last' => 1
    },

    # Show items
    {
        'Test' => sub {
            ($ENV{'SCRIPT_NAME'} eq '/cgi-bin/fcgi.pl')
            && (CGI::param('a') eq 'ShowItems')
            && CGI::param('o')
        },
        'Run' => \&PrintHTML_ObjectItems,
        'Last' => 1
    },

    # Form for item
    {
        'Test' => sub {
            ($ENV{'SCRIPT_NAME'} eq '/cgi-bin/fcgi.pl')
            && (CGI::param('a') eq 'ItemForm')
            && CGI::param('o')
        },
        'Run' => \&ItemForm,
        'Last' => 1
    },

    # JSON for item
    {
        'Test' => sub {
            ($ENV{'SCRIPT_NAME'} eq '/cgi-bin/fcgi.pl')
            && (CGI::param('a') eq 'ItemJSON')
            && CGI::param('o')
        },
        'Run' => \&ItemJSON,
        'Last' => 1
    },

    # Print CGI environment
    {
        'Test' => sub {
            ($ENV{'SCRIPT_NAME'} eq '/cgi-bin/fcgi.pl')
            && (CGI::param('a') eq 'env')
        },
        'Run' => \&PrintHTML_ENV,
        'Last' => 1
    },

    # Default
    {
        'Test' => sub { 1 },
        'Run' => sub { print "Welcome!" }
    }
);

#=========================
#  C Y C L E   B E G I N
#=========================
my $HTTPRequest = FCGI::Request();

while ($HTTPRequest->Accept() >= 0)
{

    my $Content = '';
    my $ContentType = '';

    RULE:
    for my $Rule (@URLs)
    {
        if ($Rule->{'Test'}->())
        {
            my %Result = $Rule->{'Run'}->(
                'Admin' => $Admin,
                'Template' => $Template,
                'TemplateSubs' => \%TemplateSubs,
            );

            if (
                !%Result
                || (not exists $Result{'ContentType'})
                || (not exists $Result{'Content'})
            ) {
                warn "Invalid rule: Try to guess which?";
                next RULE;
            }

            if ($ContentType && ($Result{'ContentType'} ne $ContentType))
            {
                die "Invalid content type combination";
            }
            else
            {
                $ContentType = $Result{'ContentType'};
                $Content .= $Result{'Content'};
            }

            last RULE if $Rule->{'Last'};
        }
    }

    $ContentType ||= 'text/html';

    if ($ContentType eq 'text/html')
    {
        print "Content-type: ". $ContentType ."; charset=utf8\n\n";
        print "<html>";

        $Template->process(
            'HTMLHead.html', {
                %TemplateSubs
            }
        );

        if ($Content)
        {
            print Encode::encode('utf8',$Content);
        }
        else
        {
            warn "Empty content";
        }

        print "</html>";
    }

    else
    {
        print "Content-type: ". $ContentType ."\n\n";
        print $Content if $Content;
    }
}

#=========================
#  C Y C L E   E N D
#=========================

sub PrintHTML_ObjectItems
{
    my (%Params) = @_;

    my $PageLimit = 20;

    my $Object = CGI::param('o');
    my $Page = CGI::param('page');

    my $ItemsCount = $Params{'Admin'}->Count($Object);

    my $MaxPage = int($ItemsCount / $PageLimit) + ($ItemsCount % $PageLimit ? 1 : 0);

    if (!$Page)
    {
        $Page = 1;
    }
    elsif ($Page > $MaxPage)
    {
        $Page = $MaxPage;
    }

    my @Items = $Params{'Admin'}->List($Object,
        'Offset' => (($Page - 1) * $PageLimit),
        'Limit' => $PageLimit
    );

    my @Fields = $Params{'Admin'}->Fields($Object);

    my $Output = '';

    $Params{'Template'}->process(
        "ShowItems.html",
        {
            %{ $Params{'TemplateSubs'} },
            'ObjectName' => $Object,
            'Items' => \@Items,
            'Fields' => \@Fields,
            'Page' => $Page,
            'Pages' => [1 .. $MaxPage],
        },
        \$Output
    ) or warn "Cannot process Template: ". $Params{'Template'}->error();

    return (
        'ContentType' => 'text/html',
        'Content' => $Output
    );
}

sub ItemJSON
{
    my (%Params) = @_;

    my $Object = CGI::param('o');
    my $ID = CGI::param('id');
    $ID =~ s/\D+//gs if $ID;

    my %Data = ();

    if ($ID)
    {
        %Data = $Params{'Admin'}->Get($Object,
            'ID' => $ID
        );
    }

    my $JSON = JSON::encode_json(\%Data);

    return (
        'ContentType' => 'application/json',
        'Content' => $JSON
    );
}

sub ItemForm
{
    my (%Params) = @_;

    my $Object = CGI::param('o');
    my $ID = CGI::param('id');
    $ID =~ s/\D+//gs if $ID;

    my @Fields = $Params{'Admin'}->Fields($Object);
    my %Data = ();
    my @Messages = ();

    if (CGI::param('submit'))
    {
        if (grep { $Object eq  $_ } qw(123))
        {
        }
        else
        {
            %Data = ItemFormSimple(
                'Fields' => \@Fields
            );

            if ($ID)
            {
                $Data{'ID'} = $ID;

                my $Result = $Params{'Admin'}->Update($Object,
                    %Data
                );

                if ($Result)
                {
                    push @Messages, "Item successfully updated";
                }
            }
            else
            {
                $Data{'ID'} = $Params{'Admin'}->Add($Object,
                    %Data
                );

                if ($Data{'ID'})
                {
                    push @Messages, "Item successfully added";
                }
            }
        }
    }

    elsif ($ID)
    {
        %Data = $Params{'Admin'}->Get($Object,
            'ID' => $ID
        );
        # PrintHTML_YAML(\%Data);
    }

    my @RefTemplates = $Params{'Admin'}->GetRefTemplates(
        'Class' => $Object,
        'ObjectID' => $ID,
        'Enabled' => 1,
        'CountLinks' => 1,
    );

    my $Output = '';

    $Params{'Template'}->process(
        "ItemForm.html",
        {
            %{ $Params{'TemplateSubs'} },
            'ObjectName' => $Object,
            'Fields' => \@Fields,
            'Data' => \%Data,
            'Messages' => \@Messages,
            'RefTemplates' => [@RefTemplates],
        },
        \$Output
    ) or warn "Cannot process Template: ". $Params{'Template'}->error();

    return (
        'ContentType' => 'text/html',
        'Content' => $Output
    );
}

sub ItemFormSimple
{
    my (%Params) = @_;

    my %FormData = ();

    for my $Field (@{ $Params{'Fields'} })
    {
        my $Value = Encode::decode('utf8', CGI::param($Field->{'Name'}));
        $FormData{ $Field->{'Name'} } = $Value;
    }

    return %FormData;
}

sub TopMenuSimple
{
    my (%Params) = @_;

    my @Objects = $Params{'Admin'}->GetObjects();

    my $Output = '';

    $Params{'Template'}->process(
        "TopMenuSimple.html",
        {
            %{ $Params{'TemplateSubs'} },
            'Objects' => \@Objects
        },
        \$Output
    ) or warn "Cannot process Template: ". $Params{'Template'}->error();

    return (
        'ContentType' => 'text/html',
        'Content' => $Output
    );
}

# ===========
#  Obsoleted
# ===========

sub PrintHTML_YAML
{
    my ($DataRef) = @_;

    my $YAML = YAML::XS::Dump($DataRef);

    $YAML =~ s/\n/<br>\n/gs;
    $YAML =~ s/ /&nbsp;/gs;

    print "<tt>$YAML</tt>";
}

sub PrintHTML_Objects
{
    my @Objects = sort $Admin->GetObjects();

    print "<ul>";
    for my $Object (@Objects)
    {
        print qq{<li><a href="/cgi-bin/fcgi.pl?o=$Object&a=ShowItems">$Object</a></li>};
        # $Admin->InitDBTable($Object);
    }
    print "</ul>";
}

sub PrintHTML_ENV
{
    print map { "<strong>$_</strong>: ". $ENV{ $_ } ."<br>\n" } sort keys %ENV;
}

