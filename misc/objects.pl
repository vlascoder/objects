#!/usr/bin/perl

use strict;

use Data::Dumper;

#use File::Basename;
#use FindBin qw($RealBin);
#use lib dirname($RealBin);

use lib 'modules';
use Database;
# use Object::Catalog;
# use Object::RemoteContent;
# use Object::Catalog::ItemToRemoteContent;
# use Reference::Template;
use Reference::Links;

my $DBAccount = {
    'host' => 'localhost',
    'name' => 'flowers',
    'user' => 'flowers',
    'pass' => '123'
};

Database::init({
    'main' => $DBAccount
});

my $dbh = Database::h('main')
    or die "Cannot init main DB";

# my $Object= Object::Catalog->new(
# my $Object = Object::RemoteContent->new(
# my $Object = Object::Catalog::ItemToRemoteContent->new(
# my $Object = Reference::Template->new(
my $Object = Reference::Links->new(
    'dbh' => $dbh,
);

$Object->InitDBTable(
    'CreateIfNotExists' => 1
);

