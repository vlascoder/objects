#!/usr/bin/perl

use strict;
use utf8;

use Encode;
use JSON;
use File::Path qw(make_path);
use Getopt::Long;

use Data::Dumper;
use YAML;

#use File::Basename;
#use FindBin qw($RealBin);
#use lib dirname($RealBin);

use lib 'modules';

use WebParser;
use Database;
use Object::RemoteContent;
use Object::Catalog;
use Object::Catalog::Section;
use Object::Catalog::Item;
use Object::Catalog::ItemToRemoteContent;
use Reference;

my (
    $UpdateSections,
    $ParseSections,
    $ParseItems,
);

GetOptions(
    'update_sections' => \$UpdateSections,
    'parse_sections' => \$ParseSections,
    'parse_items' => \$ParseItems,
);

if (!$UpdateSections && !$ParseSections && !$ParseItems)
{
    print qq{Usage:
    $0 [options]

Options:
    --update_sections
    --parse_sections
    --parse_items
};

    exit;
}

my $WebParser = WebParser->new();

my $DBAccount = {
    'host' => 'localhost',
    'name' => 'parser',
    'user' => 'parser',
    'pass' => '123'
};

Database::init({
    'main' => $DBAccount
});

my $dbh = Database::h('main')
    or die "Cannot init main DB";

my %CommonInitParams = (
    'dbh' => $dbh
);


my %Objects = (
    'RC'
        => Object::RemoteContent->new(%CommonInitParams),
    'Catalog'
        => Object::Catalog->new(%CommonInitParams),
    'Section'
        => Object::Catalog::Section->new(%CommonInitParams),
    'Item'
        => Object::Catalog::Item->new(%CommonInitParams),
    'ItemToRC'
        => Object::Catalog::ItemToRemoteContent->new(%CommonInitParams),
);

while (my ($ObjectName, $Class) = each %Objects)
{
    $Class->InitDBTable(
        'CreateIfNotExists' => 1
    );
}

my $References = Reference->new(
    %CommonInitParams
);

#=======================================
# Paste your code below
#=======================================

my $CatalogID = InitCatalog(
    'Name' => 'Garda'
) or die "Cannot init catalog";

$References->AddTemplate(
    'Classes' => [
        'CatalogItem',
        'CatalogSection',
    ],
    'Description' => 'Catalog: Sections <-> Items',
);

if ($UpdateSections)
{
    ParseCatalogMenu(
        'CatalogID' => $CatalogID
    );
}

if ($ParseSections)
{
    my $JSON = JSON->new();
    # $JSON->utf8(1);
    $JSON->canonical(1);

    my $Index = 0;

    while (
        my ($Section) = $Objects{'Section'}->Search(
            'CatalogID' => $CatalogID,
            'Offset' => $Index++,
            'Limit' => 1,
        )
    ) {
        ParseCatalogSection(
            'Section' => $Section,
            'JSON' => $JSON,
        );
    }
}

if ($ParseItems)
{
    my $SectionIndex = 0;

    SECTION:
    while (
        my ($Section) = $Objects{'Section'}->Search(
            'CatalogID' => $CatalogID,
            'Enabled' => 1,
            'Offset' => $SectionIndex++,
            'Limit' => 1,
        )
    ) {
        my @ItemsLinks = $References->GetLinks(
            'Objects' => {
                'CatalogSection' => [$Section->{'ID'}],
                'CatalogItem' => [],
            },
        );

        ITEM:
        for my $Link (@ItemsLinks)
        {

            my ($Item) = $Objects{'Item'}->Search(
                'ID' => $Link->{'CatalogItem'}
            );

            if (!$Item)
            {
                warn "Invalid link: ".
                    "CatalogSection[". $Link->{'CatalogSection'} ."]".
                    " <-> ".
                    "CatalogItem[". $Link->{'CatalogItem'} ."]".
                    " ".
                    "(Item not found)";

                next ITEM;
            }

            next ITEM if
                   (1 && $Item->{'ParsedAge'} < 1 * 24 * 60 * 60) # day
                || (0 && $Item->{'ParsedAge'} < 30 * 60)          # half hour
            ;

            ParseCatalogItem(
                'Item' => $Item,
            );

            sleep (1 + rand(3));
        }
    }
}


#=======================================

exit;

sub InitCatalog
{
    my (%Params) = @_;

    if ($Params{'Name'})
    {
        my @Catalogs = $Objects{'Catalog'}->Search(
            'Name' => $Params{'Name'}
        );

        if (scalar @Catalogs == 1)
        {
            return $Catalogs[0]->{'ID'};
        }
        elsif (not scalar @Catalogs)
        {
            return $Objects{'Catalog'}->Add(
                'Name' => $Params{'Name'}
            );
        }
    }
}

sub ParseCatalogMenu
{
    my (%Params) = @_;

    my $Result = $WebParser->HTTPRequest(
        'Method' => 'get',
        'URL' => 'http://www.garda-opt.ru',
    );

    if (!$Result->{'Error'})
    {
        $Result->{'Content'} =~ s/>\s+</></gs;
        $Result->{'Content'} =~ s/<!--.+?-->//gs;
        $Result->{'Content'} =~ s/<script.+?<\/script>//gs;

        my ($Footer) = $Result->{'Content'} =~ /<footer>(.+?)<\/footer>/;

        while ($Footer =~ s/<a href="([^"]+)">(.+?)<\/a>//)
        {
            my $URL = $1;
            my $Name = $2;

            if ($URL =~ /^\/catalog/)
            {
                $Name =~ s/<[^>]+>//gs;
                $Name = Encode::encode('utf8', $Name);

                my @Sections = $Objects{'Section'}->Search(
                    'Name' => $Name,
                    'CatalogID' => $Params{'CatalogID'},
                );

                if (not scalar @Sections)
                {
                    my $Time = SQLTime();

                    my $SectionID = $Objects{'Section'}->Add(
                        'CatalogID' => $Params{'CatalogID'},
                        'Name' => $Name,
                        'URL' => "http://www.garda-opt.ru" . $URL,
                        'Created' => $Time,
                        'Updated' => $Time,
                        'Enabled' => 1,
                    );

                    print "New section added: id=$SectionID, name=$Name, url=$URL\n";
                }
            }
        }
    }
}

sub ParseCatalogSection
{
    my (%Params) = @_;

    my $Result = $WebParser->HTTPRequest(
        'Method' => 'get',
        'URL' => $Params{'Section'}->{'URL'},
    );

    my $SectionCode;

    if (!$Result->{'Error'})
    {
        ($SectionCode) = $Result->{'Content'} =~ /"sectionCode":"([^"]+)"/s;
    }

    if ($SectionCode)
    {
        my $Page = 1;

        while ($Page)
        {
            my $URL = "http://www.garda-opt.ru/catalog/load-page?". join('&',
                map {
                    join('=', @$_)
                } (
                    ['loadSectionData', 'true'],
                    ['sectionCode', $SectionCode],
                    ['page', $Page],
                    ['sort', 'default%2Cdesc']
                )
            );

            my $SectionPageRequest = $WebParser->HTTPRequest(
                'Method' => 'get',
                'URL' => $URL
            );

            if (!$Result->{'Error'})
            {
                my $Data = $Params{'JSON'}->decode($SectionPageRequest->{'Content'});

                if ($Data && $Data->{'result'} && ($Data->{'result'} eq 'ok') && $Data->{'items'} && scalar(@{ $Data->{'items'} }))
                {
                    for my $Item (@{ $Data->{'items'} })
                    {
                        while (my ($Key) = each %$Item)
                        {
                            $Item->{ $Key } =~ s/^\s+|\s+$//gs;
                        }

                        my ($PriceCeil, $PriceTail) = split(/\./, $Item->{'price'});
                        $Item->{'price'} = join('.', $PriceCeil, sprintf("%02d", $PriceTail || 0));

                        $Item->{'Size'} = join('*',
                            sort { $b <=> $a }
                            map { $Item->{ $_ } }
                            qw(PROPERTY_WIDTH_VALUE PROPERTY_LENGTH_VALUE PROPERTY_HEIGHT_VALUE)
                        );

                        my %DescriptionMap = (
                            'PROPERTY_MATERIAL_VALUE' => 'Материал',
                            'Size' => 'Размеры',
                        );

                        my $URL = 'http://www.garda-opt.ru' . $Item->{'url'};

                        my %ItemData = (
                            'URL' => $URL,
                            'Artikul' => $Item->{'PROPERTY_CML2_ARTICLE_VALUE'},
                            'Name' => $Item->{'PROPERTY_NAME_SITE_VALUE'},
                            'Price' => $Item->{'price'},
                            'Stock' => $Item->{'CATALOG_QUANTITY'},
                            'Description' => join("\n", map {
                                    $Item->{ $_ } ? ($DescriptionMap{ $_ } . ": " . $Item->{ $_ }) : ()
                                } qw(
                                    PROPERTY_MATERIAL_VALUE
                                    Size
                                )
                            ),
                            'LongDescription' => ($Item->{'PREVIEW_TEXT'} || ''),
                            'SourceJSON' => $Params{'JSON'}->encode($Item),
                            'Enabled' => 1,
                        );

                        my %SearchParams = (
                            'URL' => $URL,
                        );

                        if ($Item->{'PROPERTY_CML2_ARTICLE_VALUE'})
                        {
                            $SearchParams{'Artikul'} = $Item->{'PROPERTY_CML2_ARTICLE_VALUE'};
                        }

                        my @Items = $Objects{'Item'}->Search(
                            %SearchParams
                        );

                        my $Time = SQLTime();

                        if (scalar(@Items) == 1)
                        {
                            my @DifferentParams = ();

                            $Items[0]->{ 'SourceJSON' } =
                                $Params{'JSON'}->encode(
                                    $Params{'JSON'}->decode(
                                        $Items[0]->{ 'SourceJSON' }
                                    )
                                );

                            for my $ParamName (qw(Artikul Name Price Stock Description LongDescription _SourceJSON Enabled))
                            {
                                if ($ItemData{ $ParamName } ne $Items[0]->{ $ParamName })
                                {
                                    push @DifferentParams, $ParamName;
                                }
                            }

                            if (scalar(@DifferentParams))
                            {
                                print "Item was changed (".join(', ',
                                    "id=". $Items[0]->{'ID'},
                                    "params='". join('|', @DifferentParams) ."'",
                                    ). ")\n";

                                $Objects{'Item'}->Update(
                                    %ItemData,
                                    'ID' => $Items[0]->{'ID'},
                                    'Updated' => $Time,
                                );
                            }

                            $References->AddLinks(
                                'Objects' => {
                                    'CatalogItem' => [$Items[0]->{'ID'}],
                                    'CatalogSection' => [$Params{'Section'}->{'ID'}],
                                }
                            );
                        }
                        elsif (not scalar(@Items))
                        {
                            print "New item in section '". Encode::encode('utf8', $Params{'Section'}->{'Name'}) ."'\n";

                            my $ItemID = $Objects{'Item'}->Add(
                                %ItemData,
                                'Created' => $Time,
                                'Updated' => $Time,
                            );

                            if ($ItemID)
                            {
                                $References->AddLinks(
                                    'Objects' => {
                                        'CatalogItem' => [$ItemID],
                                        'CatalogSection' => [$Params{'Section'}->{'ID'}],
                                    }
                                );
                            }
                        }
                        else
                        {
                            warn "Found similar items: ".join(', ', map { $_->{'ID'} } @Items);
                        }
                    }
                }

                if ($Data->{'noMorePages'})
                {
                    $Page = 0;
                }
                else
                {
                    $Page++;
                }
            }
            else
            {
                $Page = 0;
            }

            sleep (1 + rand(6)) if $Page;
        }
    }
}

sub ParseCatalogItem
{
    my (%Params) = @_;

    print "Item: ". $Params{'Item'}->{'ID'} ."\n";

    my $Result = $WebParser->HTTPRequest(
        'Method' => 'get',
        'URL' => $Params{'Item'}->{'URL'},
    );

    if (!$Result->{'Error'})
    {
        my ($Images) = $Result->{'Content'} =~ /<div class="main-photo-wrapper">(.+?)<\/div>/s;

        if ($Images)
        {
            my @Links = $Images =~ /href="([^"]+)"/gs;

            SaveCatalogItemRC(
                'Links' => \@Links,
                'ItemID' => $Params{'Item'}->{'ID'}
            );

            my $Time = SQLTime();

            $Objects{'Item'}->Update(
                'ID' => $Params{'Item'}->{'ID'},
                'Parsed' => $Time,
                'Updated' => $Time,
            );
        }
    }

    elsif ($Result->{'Code'} eq '404')
    {
        my $Time = SQLTime();

        $Objects{'Item'}->Update(
            'ID' => $Params{'Item'}->{'ID'},
            'Enabled' => 0,
            'Invalid' => 1,
            'Parsed' => $Time,
            'Updated' => $Time,
        );
    }

    else
    {
        warn $Result->{'Error'};
    }
}

sub SaveCatalogItemRC
{
    my (%Params) = @_;

    my $TemplateID = $References->AddTemplate(
        'Classes' => [
            'CatalogItem',
            'RemoteContent',
        ],
        'Token' => 'Gallery',
        'Description' => 'Catalog: Item gallery',
    );

    if (!$TemplateID)
    {
        warn "Invalid template";
        return;
    }

    my @ItemRCLinks = $References->GetLinks(
        'Objects' => {
            'CatalogItem' => [$Params{'ItemID'}],
            'RemoteContent' => []
        },
        'Token' => 'Gallery',
    );

    # Save old references {
    my @ItemToRCRefs = $Objects{'ItemToRC'}->Search(
        'ItemID' => $Params{'ItemID'},
    );

    for my $Ref (@ItemToRCRefs)
    {
        if (not grep { $_->{'RemoteContent'} eq $Ref->{'RemoteContentID'} } @ItemRCLinks)
        {
            $References->AddLinks(
                'Objects' => {
                    'CatalogItem' => [$Params{'ItemID'}],
                    'RemoteContent' => [$Ref->{'RemoteContentID'}],
                },
                'Token' => 'Gallery',
                'Enabled' => $Ref->{'Enabled'}
            );
        }
    }

    @ItemRCLinks = $References->GetLinks(
        'Objects' => {
            'CatalogItem' => [$Params{'ItemID'}],
            'RemoteContent' => []
        },
        'Token' => 'Gallery',
    );
    # }

    my %EnabledRC = {};

    for my $Link (@{ $Params{'Links'} })
    {
        $Link =~ s/^\/\//http:\/\//s;

        my $Tail = '';

        if ($Link =~ s/\?(.*)$//s)
        {
            $Tail = $1;
        }

        my ($RC) = $Objects{'RC'}->Search(
            'URL' => $Link,
        );

        my $RemoteContentID = undef;

        if ($RC)
        {
            $RemoteContentID = $RC->{'ID'};
        }
        else
        {
            $RemoteContentID = $Objects{'RC'}->Add(
                'URL' => $Link,
            );
        }

        if ($RemoteContentID)
        {
            if (grep { $_->{'RemoteContent'} eq $RemoteContentID } @ItemRCLinks)
            {
                $EnabledRC{ $RemoteContentID } = undef;
            }
            else
            {
                $References->AddLinks(
                    'Objects' => {
                        'CatalogItem' => [$Params{'ItemID'}],
                        'RemoteContent' => [$RemoteContentID],
                    },
                    'Token' => 'Gallery',
                    'Enabled' => 1,
                );
            }
        }
    }

    # Enable/Disable link
    for my $Link (@ItemRCLinks)
    {
        if (
            $Link->{'Enabled'}
            && (not exists $EnabledRC{ $Link->{'RemoteContent'} })
        ){
            $References->UpdateLinks(
                'Objects' => {
                    'CatalogItem' => [ $Params{'ItemID'} ],
                    'RemoteContent' => [ $Link->{'RemoteContent'} ],
                },
                'Token' => 'Gallery',
                'Enabled' => 0,
            );
        }
        elsif (
            !$Link->{'Enabled'}
            && (exists $EnabledRC{ $Link->{'RemoteContent'} })
        ){
            $References->UpdateLinks(
                'Objects' => {
                    'CatalogItem' => [ $Params{'ItemID'} ],
                    'RemoteContent' => [ $Link->{'RemoteContent'} ],
                },
                'Token' => 'Gallery',
                'Enabled' => 1,
            );
        }
    }
}

sub SQLTime
{
    my $Time = sprintf(
        "%04d-%02d-%02d %02d:%02d:%02d",
        (localtime)[5] + 1900,
        (localtime)[4] + 1,
        (localtime)[3,2,1,0]
    );
}
