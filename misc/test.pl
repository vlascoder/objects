#!/usr/bin/perl

use strict;

use Data::Dumper;

#use File::Basename;
#use FindBin qw($RealBin);
#use lib dirname($RealBin);

use lib 'modules';
use Shop;

my $DBAccount = {
    'host' => 'localhost',
    'name' => 'flowers',
    'user' => 'flowers',
    'pass' => '123'
};

my $Shop = Shop->new(
    'DBAccount' => {
        'main' => $DBAccount
    }
);


# TestBouquet($Shop);

#=c
my @Objects = $Shop->GetObjects();

for my $Object (@Objects)
{
    $Shop->InitDBTable($Object);
}
#=cut


sub TestFlowers
{
    my ($Shop) = @_;

    Flowers();

    if (0)
    {
        $Shop->Add('Flower',
            'Title' => 'Rose',
            'Colors' => 'White, yellow',
            'Price' => 205.4,
        );
    }

    if (0)
    {
        for my $ID (qw(1 2 3 4 5 6 7))
        {
            $Shop->Delete('Flower',
                'ID' => $ID,
            );
        }
    }

    if (0)
    {
        $Shop->Update('Flower',
            'ID' => 8,
            'Price' => 401.7,
            # 'Title' => 'Rose is good',
        );
    }

    Flowers();
}

sub Flowers
{
    my @Flowers = $Shop->List('Flower');

    use Data::Dumper;
    warn Dumper(\@Flowers);
}

sub TestBouquet
{
    my ($Shop) = @_;

    my ($Bouquet) = $Shop->Search('Bouquet',
        'Artikul' => 'R11',
    );

    if (!$Bouquet)
    {
        $Shop->Add('Bouquet',
            'Artikul' => 'R11',
            'Title' => '11 Roses',
        );
    }

    if ($Bouquet)
    {
        warn Dumper($Bouquet);

        my @FlowerRefs = $Shop->GetReferences(
            'SourceType' => 'Bouquet',
            'SourceID' => $Bouquet->{'ID'},
            'DestinationType' => 'Flower',
            # 'Direction' => 'Forward',
        );

        # Print references
        if (scalar @FlowerRefs)
        {
            warn Dumper(\@FlowerRefs);
        }

        # Create refs
        else
        {
            my ($Rose) = $Shop->Search('Flower',
                'Title' => 'Rose',
            );

            if ($Rose)
            {
                warn $Rose->{'Price'};

                $Shop->AddReference(
                    'SourceType' => 'Bouquet',
                    'SourceID' => $Bouquet->{'ID'},
                    'DestinationType' => 'Flower',
                    'DestinationID' => $Rose->{'ID'},
                );

                $Shop->Update('Bouquet',
                    'ID' => $Bouquet->{'ID'},
                    'Price' => 11 * $Rose->{'Price'}
                );
            }

            else
            {
                warn "Cannot find any rose";
            }
        }
    }
}

