#!/usr/bin/perl

use strict;
use utf8;

use Encode;
use JSON;
use File::Path qw(make_path);
use Getopt::Long;

use Data::Dumper;
use YAML;

use FindBin qw($RealBin);
use File::Basename;
use lib File::Basename::dirname($RealBin)."/modules";

use WebParser;
use Database;
use Object::RemoteContent;
use Reference;

my $WebParser = WebParser->new();

my $dbh = Database::InitDB(
    'ConfigFile' => "$RealBin/db.json"
)
or die "Cannot init DB";


my %CommonInitParams = (
    'dbh' => $dbh
);


my %Objects = (
    'RC'
        => Object::RemoteContent->new(%CommonInitParams),
);

my $References = Reference->new(
    %CommonInitParams
);

#=======================================
# Paste your code below
#=======================================

my $RCIndex = 0;

# warn "Start download RC";

RC:
while (
    my ($RC) = $Objects{'RC'}->Search(
        'ContentSizeIsNull' => 1,
        #'ContentSize' => 0,
        'Invalid' => 0,
        'Offset' => $RCIndex++,
        'Limit' => 1,
        'Verbose' => 0,
    )
) {
    # next if $RC->{'URL'} =~ /garda-opt/;

    next RC if not CheckRC(
        'RC' => $RC
    );

    warn "Search similar RC: ".$RC->{'ID'};

    my $Found = FindSimilarRC(
        'CacheDir' => '/home/sa/files',
        'RC' => $RC
    );

    if ($Found < 0)
    {
        $Objects{'RC'}->Update(
            'ID' => $RC->{'ID'},
            'Invalid' => 1,
        );
    }

    if ($Found)
    {
        $RCIndex--;
        next RC;
    }

    my $Downloaded = DownloadRC(
        'CacheDir' => '/home/sa/files',
        'RC' => $RC
    );

    if ($Downloaded < 0)
    {
        $Objects{'RC'}->Update(
            'ID' => $RC->{'ID'},
            'Invalid' => 1,
        );
    }

    if ($Downloaded)
    {
        $RCIndex--;
    }

    sleep rand(3);
}


#=======================================

exit;

=item CheckRC

    Check for enabled relations

=cut
sub CheckRC
{
    my (%Params) = @_;

    my @ItemRCLinks = $References->GetLinks(
        'Objects' => {
            'CatalogItem' => [],
            'RemoteContent' => [$Params{'RC'}->{'ID'}]
        },
        'Token' => 'Gallery',
    );

    my $Valid = undef;

    if (scalar @ItemRCLinks)
    {
        LINK:
        for my $Link (@ItemRCLinks)
        {
            if ($Link->{'Enabled'})
            {
                $Valid = 1;
                last LINK;
            }
        }

    }

    # warn "CheckRC: id=".$Params{'RC'}->{'ID'}.", valid=".$Valid;

    return $Valid;
}

sub FindSimilarRC
{
    my (%Params) = @_;

    my ($Filename) = $Params{'RC'}->{'URL'} =~ /([^\/]+)$/;
    # print "$Filename\n";

    my @SimilarRCs =
        sort {
            $a->{'ID'} <=> $b->{'ID'}
        }
        grep {
            ($_->{'ID'} ne $Params{'RC'}->{'ID'})
            && ($_->{'ContentSize'} > 0)
        }
        $Objects{'RC'}->Search(
            'URLRegExp' => '/'. $Filename .'$',
        );

    # print YAML::Dump(\@SimilarRCs);
    my $Found = undef;

    if (scalar @SimilarRCs)
    {
        my $Result = $WebParser->HTTPRequest(
            'Method' => 'head',
            'URL' => $Params{'RC'}->{'URL'},
            'Headers' => 1,
        );

        if ($Result->{'Error'})
        {
            warn $Result->{'Error'};

            $Found = -1;
        }
        else
        {
            my $ContentSize = $Result->{'Headers'}->{'Content-Length'};

            SIMILAR_RC:
            for my $SimilarRC (@SimilarRCs)
            {
                if ($SimilarRC->{'ContentSize'} eq $ContentSize)
                {
                    warn "Similar RC found";
                    warn YAML::Dump($SimilarRC);

                    $Found = 1;

                    $Objects{'RC'}->Update(
                        'ID' => $Params{'RC'}->{'ID'},
                        'ContentType' => $SimilarRC->{'ContentType'},
                        'ContentSize' => $SimilarRC->{'ContentSize'},
                        'Filename' => $SimilarRC->{'Filename'},
                    );

                    last SIMILAR_RC;
                }
            }
        }
    }

    # warn ($Found ? ($Found < 0 ? "Invalid RC" : "Similar RC found") : "Similar RC not found");

    return $Found;
}

sub DownloadRC
{
    my (%Params) = @_;

    if (!$Params{'CacheDir'} || (!-d $Params{'CacheDir'}))
    {
        warn "Invalid CacheDir";
        return;
    }

    my $TestFile = $Params{'CacheDir'}."/".join('.', time, 'test');

    open (TEST, '>', $TestFile)
        or die "Cannot create test file";

    print (TEST '1')
        or die "Cannot write to test file";

    close (TEST)
        or die "Cannot close test file";

    unlink ($TestFile)
        or die "Cannot delete test file";

    my $Downloaded = undef;

    if ($Params{'RC'})
    {
        my $FileName = sprintf("%010d", $Params{'RC'}->{'ID'});

        my @FilePath = $FileName =~ /^(.+)(..)(..)(..)..$/;
        my $FullFilePath = join('/', $Params{'CacheDir'}, @FilePath);

        print $Params{'RC'}->{'URL'}, "\n";

        my $Result = $WebParser->HTTPRequest(
            'Method' => 'get',
            'URL' => $Params{'RC'}->{'URL'},
            'Headers' => 1,
        );

        if ($Result->{'Error'})
        {
            warn $Result->{'Error'};

            $Downloaded = -1;
        }
        else
        {
            if ($Result->{'Headers'}->{'Content-Type'} eq 'image/jpeg')
            {
                $FileName .= ".jpg";
            }

            WriteFile(
                'Path' => $FullFilePath,
                'Filename' => $FileName,
                'Content' => $Result->{'Content'},
            );

            $Objects{'RC'}->Update(
                'ID' => $Params{'RC'}->{'ID'},
                'ContentType' => $Result->{'Headers'}->{'Content-Type'},
                'ContentSize' => $Result->{'Headers'}->{'Content-Length'},
                'Filename' => $FileName,
            );

            $Downloaded = 1;
        }
    }

    return $Downloaded;
}

sub WriteFile
{
    my (%Params) = @_;

    my $FullFilePath = $Params{'Path'};
    my $FileName = $Params{'Filename'};

    if (!-d $FullFilePath)
    {
        File::Path::make_path($FullFilePath)
            or die "Cannot create path: $FullFilePath";
    }

    open (FILE, '>', $FullFilePath . '/' . $FileName)
        or die ("Cannot open file: ". $FullFilePath . '/' . $FileName);

    binmode FILE;

    print FILE $Params{'Content'}
        or die ("Cannot write file: ". $FullFilePath . '/' . $FileName);

    close FILE
        or die ("Cannot close file: ". $FullFilePath . '/' . $FileName);
}
