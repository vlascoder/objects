#!/usr/bin/perl

use strict;
use utf8;

use Encode;
use File::Path qw(make_path);
use Getopt::Long;

use Data::Dumper;
use YAML;

use FindBin qw($RealBin);
use File::Basename;
use lib File::Basename::dirname($RealBin)."/modules";

use WebParser::LoftArt;
use Database;
use Object::RemoteContent;
use Object::Catalog;
use Object::Catalog::Section;
use Object::Catalog::Item;
use Object::Catalog::ItemToRemoteContent;
use Reference;

my (
    $doUpdateSections,
    $doParseSections,
    $doParseItems,
);

GetOptions(
    'update_sections' => \$doUpdateSections,
    'parse_sections' => \$doParseSections,
    'parse_items' => \$doParseItems,
);

if (!$doUpdateSections && !$doParseSections && !$doParseItems)
{
    print qq{Usage:
    $0 [options]

Options:
    --update_sections
    --parse_sections
    --parse_items
};

    exit;
}


my $dbh = Database::InitDB(
    'ConfigFile' => "$RealBin/db.json"
)
or die "Cannot init DB";


my %CommonInitParams = (
    'dbh' => $dbh
);


my %Objects = (
    'RC'
        => Object::RemoteContent->new(%CommonInitParams),
    'Catalog'
        => Object::Catalog->new(%CommonInitParams),
    'Section'
        => Object::Catalog::Section->new(%CommonInitParams),
    'Item'
        => Object::Catalog::Item->new(%CommonInitParams),
);

while (my ($ObjectName, $Class) = each %Objects)
{
    $Class->InitDBTable(
        'CreateIfNotExists' => 1
    );
}

my $References = Reference->new(
    %CommonInitParams
);

$References->AddTemplate(
    'Classes' => [
        'Catalog',
        'CatalogSection',
    ],
    'Description' => 'Catalog: Catalog <-> Sections',
)
or die "Cannot create reference template";

$References->AddTemplate(
    'Classes' => [
        'CatalogItem',
        'CatalogSection',
    ],
    'Description' => 'Catalog: Sections <-> Items',
)
or die "Cannot create reference template";

$References->AddTemplate(
    'Classes' => [
        'CatalogItem',
        'RemoteContent',
    ],
    'Token' => 'Gallery',
    'Description' => 'Catalog: Item gallery',
)
or die "Cannot create reference template";

my $WebParser = WebParser::LoftArt->new();

#=======================================
# Paste your code below
#=======================================

my $CatalogID = InitCatalog(
    'Name' => 'LoftArt'
) or die "Cannot init catalog";

if ($doUpdateSections)
{
    ParseCatalogMenu(
        'CatalogID' => $CatalogID,
        'Crawler' => $WebParser
    );
}

if ($doParseSections)
{
    my @CatalogSectionsList = $References->GetLinks(
        'Objects' => {
            'CatalogSection' => [],
            'Catalog' => [ $CatalogID ],
        },
    );

    my %CatalogSections =
        map {
            $_->{'CatalogSection'} => $_
        }
        grep {
            $_->{'Enabled'}
        }
        @CatalogSectionsList;

    while (my ($SectionID) = each %CatalogSections)
    {
        ParseCatalogSection(
            'SectionID' => $SectionID,
            'Crawler' => $WebParser
        );

        sleep(10);
    }
}

if ($doParseItems)
{
    my @CatalogToSectionLinks = $References->GetLinks(
        'Objects' => {
            'CatalogSection' => [],
            'Catalog' => [ $CatalogID ],
        },
    );

    C2S_LINK:
    for my $C2S_Link (@CatalogToSectionLinks)
    {
        next C2S_LINK if !$C2S_Link->{'Enabled'};

        my %SectionData = $Objects{'Section'}->Get(
            'ID' => $C2S_Link->{'CatalogSection'}
        );

        next C2S_LINK if !$SectionData{'Enabled'};

        my @SectionToItemLinks = $References->GetLinks(
            'Objects' => {
                'CatalogSection' => [ $C2S_Link->{'CatalogSection'} ],
                'CatalogItem' => [ ],
            },
        );

        S2I_LINK:
        for my $S2I_Link (@SectionToItemLinks)
        {
            my $Result = ParseCatalogItem(
                'ItemID' => $S2I_Link->{'CatalogItem'},
                'Crawler' => $WebParser
            );

            next S2I_LINK if ($Result && ($Result eq 'Skip'));

            # last S2I_LINK;

            sleep(10);
        }

        # last C2S_LINK;
    }
}

if (0)
{
    # Clean invalid RC and reparse related items {
    my $InvalidItems = $dbh->selectcol_arrayref(
        "SELECT i.id
        FROM catalog_items i
          JOIN references_links r1 ON r1.ref_tmpl_id = 3
            AND r1.from_id = i.id
          JOIN remote_content rc ON rc.id = r1.to_id
        WHERE rc.invalid = 1
        GROUP BY i.id"
    );

    if (scalar @$InvalidItems)
    {
        $dbh->do("UPDATE catalog_items SET parsed = NOW() - INTERVAL 7 DAY WHERE id IN (".join(',', grep { /^\d+$/ } @$InvalidItems).")");
    }

    while (1)
    {
        my $InvalidRC = $dbh->selectcol_arrayref(
            "SELECT id FROM remote_content WHERE invalid = 1 LIMIT 10"
        );

        last if not scalar @$InvalidRC;

        for my $ID (@$InvalidRC)
        {
            $dbh->do("DELETE FROM remote_content WHERE id = ?", undef, $ID);
            $dbh->do("DELETE FROM references_links WHERE ref_tmpl_id = 3 AND to_id = ?", undef, $ID);
        }
    }
    # }

    my $SectionIndex = 0;

    SECTION:
    while (
        my ($Section) = $Objects{'Section'}->Search(
            'CatalogID' => $CatalogID,
            'Enabled' => 1,
            'Offset' => $SectionIndex++,
            'Limit' => 1,
        )
    ) {
        my @ItemsLinks = $References->GetLinks(
            'Objects' => {
                'CatalogSection' => [$Section->{'ID'}],
                'CatalogItem' => [],
            },
        );

        ITEM:
        for my $Link (@ItemsLinks)
        {

            my ($Item) = $Objects{'Item'}->Search(
                'ID' => $Link->{'CatalogItem'}
            );

            if (!$Item)
            {
                warn "Invalid link: ".
                    "CatalogSection[". $Link->{'CatalogSection'} ."]".
                    " <-> ".
                    "CatalogItem[". $Link->{'CatalogItem'} ."]".
                    " ".
                    "(Item not found)";

                next ITEM;
            }

            next ITEM if
                   (0 && $Item->{'ParsedAge'} && $Item->{'ParsedAge'} < 1 * 24 * 60 * 60) # day
                || (0 && $Item->{'ParsedAge'} && $Item->{'ParsedAge'} < 7 * 24 * 60 * 60) # week
                || (1 && $Item->{'ParsedAge'} && $Item->{'ParsedAge'} < 30 * 24 * 60 * 60) # month
                || (0 && $Item->{'ParsedAge'} && $Item->{'ParsedAge'} < 30 * 60)          # half hour
                || (0 && $Item->{'ParsedAge'} && $Item->{'ParsedAge'} < 2 * 60 * 60)      # 2 hours
            ;

            ParseCatalogItem(
                'Item' => $Item,
            );

            sleep (1 + rand(3));
        }
    }
}


#=======================================

exit;

sub InitCatalog
{
    my (%Params) = @_;

    if ($Params{'Name'})
    {
        my @Catalogs = $Objects{'Catalog'}->Search(
            'Name' => $Params{'Name'}
        );

        if (scalar @Catalogs == 1)
        {
            return $Catalogs[0]->{'ID'};
        }
        elsif (not scalar @Catalogs)
        {
            return $Objects{'Catalog'}->Add(
                'Name' => $Params{'Name'}
            );
        }
    }
}

sub ParseCatalogMenu
{
    my (%Params) = @_;

    my @CatalogSectionsList = $References->GetLinks(
        'Objects' => {
            'CatalogSection' => [],
            'Catalog' => [ $Params{'CatalogID'} ],
        },
    );

    my %CatalogSections =
        map {
            $_->{'CatalogSection'} => $_
        }
        grep {
            $_->{'Enabled'}
        }
        @CatalogSectionsList;

    # Get array of hashes { 'Name' => ..., 'URL' => ... }
    my @NewSections = $Params{'Crawler'}->GetAllCatalogSections();

    SECTION:
    for my $Section (@NewSections)
    {
        # my $IsParent = grep { $_->{'URL'} =~ /^$Section->{'URL'}./ } @Sections;
        # next SECTION if $IsParent;

        my @Sections = $Objects{'Section'}->Search(
            'URL' => $Section->{'URL'},
        );

        if (scalar @Sections)
        {
            for my $Section (@Sections)
            {
                delete $CatalogSections{ $Section->{'ID'} };
            }
        }
        else
        {
            my $Time = SQLTime();

            my $SectionID = $Objects{'Section'}->Add(
                'CatalogID' => $Params{'CatalogID'},
                'Name' => $Section->{'Name'},
                'URL' => $Section->{'URL'},
                'Created' => $Time,
                'Updated' => $Time,
                'Enabled' => 1,
            );

            $References->AddLinks(
                'Objects' => {
                    'Catalog' => [$Params{'CatalogID'}],
                    'CatalogSection' => [$SectionID],
                }
            );

            printf("New section added: id=$SectionID, name=%s, url=%s\n", $Section->{'Name'}, $Section->{'URL'});
        }
    }

    # Disable obsoleted sections and its links to catalog
    for my $Link (@CatalogSectionsList)
    {
        if (exists $CatalogSections{ $Link->{'CatalogSection'} })
        {
            my $Time = SQLTime();

            $Objects{'Section'}->Update(
                'ID' => $Link->{'CatalogSection'},
                'Enabled' => 0,
                'Updated' => $Time,
            );

            $References->UpdateLinks(
                'Objects' => {
                    'Catalog' => [ $Params{'CatalogID'} ],
                    'CatalogSection' => [ $Link->{'CatalogSection'} ],
                },
                'Enabled' => 0,
            );
        }
    }
}

sub ParseCatalogSection
{
    my (%Params) = @_;

    my %SectionData = $Objects{'Section'}->Get(
        'ID' => $Params{'SectionID'}
    );

    # Get array of hashes { 'Name' => ..., 'URL' => ... }
    my @NewItems = $Params{'Crawler'}->GetAllSectionItems(
        'URL' => $SectionData{'URL'}
    );

    ITEM:
    for my $Item (@NewItems)
    {
        my %SearchParams = (
            'URL' => $Item->{'URL'},
        );

        my ($SavedItem, @OtherItems) = $Objects{'Item'}->Search(
            %SearchParams
        );

        my $Time = SQLTime();

        # One item
        if ($SavedItem && !scalar(@OtherItems))
        {
            my %Result = CmpHash(
                'Original' => $SavedItem,
                'New' => $Item,
                'Keys' => [ (grep { !/^(?:URL|Dimensions)$/ } keys %$Item) ],
            );

            if (scalar(@{ $Result{'Diff'} }))
            {
                print "Item was changed (id='". $SavedItem->{'ID'} ."', ". $Result{'Log'} .")";

                $Objects{'Item'}->Update(
                    %$Item,
                    'ID' => $SavedItem->{'ID'},
                    'Updated' => $Time,
                );
            }

            $References->AddLinks(
                'Objects' => {
                    'CatalogItem' => [ $SavedItem->{'ID'} ],
                    'CatalogSection' => [$Params{'SectionID'}],
                }
            );
        }

        elsif (!$SavedItem)
        {
            print "New item in section '". Encode::encode('utf8', $SectionData{'Name'}) ."'\n";

            my $ItemID = $Objects{'Item'}->Add(
                %$Item,
                'Created' => $Time,
                'Updated' => $Time,
            );

            if ($ItemID)
            {
                $References->AddLinks(
                    'Objects' => {
                        'CatalogItem' => [$ItemID],
                        'CatalogSection' => [$Params{'SectionID'}],
                    }
                );
            }
        }
        else
        {
            warn "Found similar items: ".join(', ', map { $_->{'ID'} } ($SavedItem, @OtherItems));
        }
    }
}

sub ParseCatalogItem
{
    my (%Params) = @_;

    my %ItemData = $Objects{'Item'}->Get(
        'ID' => $Params{'ItemID'}
    );

    if (
        $ItemData{'ParsedAge'}
        && $ItemData{'ParsedAge'} < 7 * 24 * 60 * 60
    ){
        return 'Skip';
    }

    my %NewItemData = $Params{'Crawler'}->GetItemData(
        'URL' => $ItemData{'URL'}
    );

=c
    use Data::Dumper;
    warn Dumper(\%NewItemData);
    return;
=cut

    if (%NewItemData)
    {
        my %Result = CmpHash(
            'Original' => { %ItemData },
            'New' => { %NewItemData },
            'Keys' => [ (grep { !/^(?:Images)$/ } (keys %NewItemData)) ],
        );

        my $Time = SQLTime();

        if (scalar(@{ $Result{'Diff'} }))
        {
            print "Item was changed (id='". $ItemData{'ID'} ."', ". $Result{'Log'} .")\n";

            $Objects{'Item'}->Update(
                %NewItemData,
                'ID' => $ItemData{'ID'},
                'Parsed' => $Time,
                'Updated' => $Time,
            );
        }

        elsif ($NewItemData{'Crawler::NotFound'})
        {
            $Objects{'Item'}->Update(
                'ID' => $ItemData{'ID'},
                'Parsed' => $Time,
                'Updated' => $Time,
                'Enabled' => 0,
                'Invalid' => 1,
            );
        }

        if (
            ($NewItemData{'Images'})
            && (ref($NewItemData{'Images'}) eq 'ARRAY')
            && (scalar @{ $NewItemData{'Images'} })
        ){
=c
            my @ImageIDs = RC_GetIDByURL(
                'URLs' => $NewItemData{'Images'}
            );
=cut
            SaveCatalogItemRC(
                'Links' => $NewItemData{'Images'},
                'ItemID' => $ItemData{'ID'},
            );
        }
    }
}

sub ParseCatalogItem_OLD
{
    my (%Params) = @_;

    # print "Item: ". $Params{'Item'}->{'ID'} ."\n";

    my $Result = $WebParser->HTTPRequest(
        'Method' => 'get',
        'URL' => $Params{'Item'}->{'URL'},
    );

    if (!$Result->{'Error'})
    {
        my $HTML = $Result->{'Content'};
        $HTML =~ s/<(noscript|script|style).+?<\/\1>//gs;
        $HTML =~ s/<!--.+?-->/ /gs;
        $HTML =~ s/<(link|meta)[^>]+>//gs;
        $HTML =~ s/>\s+/>/gs;
        $HTML =~ s/\s+</</gs;

        my ($Artikul) = $HTML =~ /<div.+?class="article">.+?<span class="val">([^<]+)/;
        my ($LongDescription) = $HTML =~ /<section.+?itemprop="description">(.+?)<\/section>/;

        my (@Images) = $HTML =~ /<a\s+href="([^"]+)"\s+rel='gallery'>/gs;

        if (scalar @Images)
        {
            SaveCatalogItemRC(
                'Links' => \@Images,
                'ItemID' => $Params{'Item'}->{'ID'}
            );
        }

        my $Time = SQLTime();

        $Objects{'Item'}->Update(
            'ID' => $Params{'Item'}->{'ID'},
            'Artikul' => $Artikul,
            'LongDescription' => $LongDescription,
            'Parsed' => $Time,
            'Updated' => $Time,
        );

        if (0)
        {
            $HTML =~ s/>/>\n/gs;
            print "===\n$HTML\n===\n\n";
        }
        elsif (0)
        {
            print "$Artikul\n$LongDescription\n".join("\n", @Images)."\n";
        }
    }

    elsif ($Result->{'Code'} eq '404')
    {
        my $Time = SQLTime();

        $Objects{'Item'}->Update(
            'ID' => $Params{'Item'}->{'ID'},
            'Enabled' => 0,
            'Invalid' => 1,
            'Parsed' => $Time,
            'Updated' => $Time,
        );
    }

    else
    {
        warn $Result->{'Error'};
    }
}

#=======================================
#  System subs
#=======================================
sub RC_GetIDByURL
{
    my (%Params) = @_;

    my @IDs = ();

    for my $URL (@{ $Params{'URLs'} })
    {
        $URL =~ s/^\/\//http:\/\//s;

        my ($RC) = $Objects{'RC'}->Search(
            'URL' => $URL,
        );

        if ($RC)
        {
            push @IDs, $RC->{'ID'};
        }
        else
        {
            my $ID = $Objects{'RC'}->Add(
                'URL' => $URL,
                'Invalid' => 0,
            );

            push @IDs, $ID if $ID;
        }
    }

    return @IDs;
}

sub SaveCatalogItemRC
{
    my (%Params) = @_;

    my @ItemRCLinks = $References->GetLinks(
        'Objects' => {
            'CatalogItem' => [$Params{'ItemID'}],
            'RemoteContent' => []
        },
        'Token' => 'Gallery',
    );

    my %EnabledRC = {};

    for my $Link (@{ $Params{'Links'} })
    {
        $Link =~ s/^\/\//http:\/\//s;

        my $Tail = '';

        if ($Link =~ s/\?(.*)$//s)
        {
            $Tail = $1;
        }

        my ($RC) = $Objects{'RC'}->Search(
            'URL' => $Link,
        );

        my $RemoteContentID = undef;

        if ($RC)
        {
            $RemoteContentID = $RC->{'ID'};
        }
        else
        {
            $RemoteContentID = $Objects{'RC'}->Add(
                'URL' => $Link,
                'Invalid' => 0,
            );
        }

        if ($RemoteContentID)
        {
            if (grep { $_->{'RemoteContent'} eq $RemoteContentID } @ItemRCLinks)
            {
                $EnabledRC{ $RemoteContentID } = undef;
            }
            else
            {
                $References->AddLinks(
                    'Objects' => {
                        'CatalogItem' => [$Params{'ItemID'}],
                        'RemoteContent' => [$RemoteContentID],
                    },
                    'Token' => 'Gallery',
                    'Enabled' => 1,
                );
            }
        }
    }

    # Enable/Disable link
    for my $Link (@ItemRCLinks)
    {
        if (
            $Link->{'Enabled'}
            && (not exists $EnabledRC{ $Link->{'RemoteContent'} })
        ){
            $References->UpdateLinks(
                'Objects' => {
                    'CatalogItem' => [ $Params{'ItemID'} ],
                    'RemoteContent' => [ $Link->{'RemoteContent'} ],
                },
                'Token' => 'Gallery',
                'Enabled' => 0,
            );
        }
        elsif (
            !$Link->{'Enabled'}
            && (exists $EnabledRC{ $Link->{'RemoteContent'} })
        ){
            $References->UpdateLinks(
                'Objects' => {
                    'CatalogItem' => [ $Params{'ItemID'} ],
                    'RemoteContent' => [ $Link->{'RemoteContent'} ],
                },
                'Token' => 'Gallery',
                'Enabled' => 1,
            );
        }
    }
}

sub SQLTime
{
    my $Time = sprintf(
        "%04d-%02d-%02d %02d:%02d:%02d",
        (localtime)[5] + 1900,
        (localtime)[4] + 1,
        (localtime)[3,2,1,0]
    );
}

sub CmpHash
{
    my (%Params) = @_;

    my @DifferentParams = ();

    for my $ParamName (@{ $Params{'Keys'} })
    {
        my $OriginalValue = Encode::encode('utf8', $Params{'Original'}->{ $ParamName });
        my $NewValue = Encode::encode('utf8', $Params{'New'}->{ $ParamName });

        if ($OriginalValue ne $NewValue)
        {
            push @DifferentParams, $ParamName;
        }
    }

    my $Log;

    if (scalar @DifferentParams)
    {
        $Log = join(
            '; ',
            map {
                "'$_': ".
                join(
                    ' > ',
                    map {
                        "'$_'"
                    } (
                        $Params{'Original'}->{ $_ },
                        $Params{'New'}->{ $_ }
                    )
                )
            }
            @DifferentParams
        );
    }

    return (
        'Diff' => \@DifferentParams,
        'Log' => $Log
    );
}

