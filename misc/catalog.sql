
-- Кол-во связей Section - Item 
select count(*) from references_links WHERE ref_tmpl_id = 2;

-- Проверка на наличие одинаковых товаров
select url, count(*) cnt from catalog_items group by url having cnt > 1;

-- Связи одного товара с несколькими разделами
SELECT from_id, GROUP_CONCAT(to_id), COUNT(*) cnt FROM references_links WHERE ref_tmpl_id = 2 GROUP BY from_id HAVING cnt > 1;

-- Поиск сломанных ссылок (товары)
SELECT r.*
FROM references_links r
  LEFT JOIN catalog_sections s ON s.id = r.to_id
  LEFT JOIN catalog_items i ON i.id = from_id
WHERE r.ref_tmpl_id = 2
  AND i.id IS NULL;

-- Поиск сломанных ссылок (разделы)
SELECT *
FROM references_links r
  LEFT JOIN catalog_sections s ON s.id = r.to_id
WHERE r.ref_tmpl_id = 2
  AND s.id IS NULL;

-- Разделы каталога с кол-вом товаров
SELECT s.id, s.name, s.enabled, COUNT(l.id)
FROM catalog_sections s
  JOIN references_links l ON l.to_id = s.id
--  JOIN catalog_items i ON i.id = l.from_id
WHERE s.catalog_id = 1
GROUP BY s.id;

-- Disable references for invalid items
UPDATE references_links
SET enabled = 0
WHERE ref_tmpl_id = 4
  AND from_id IN (
    SELECT id
    FROM catalog_items
    WHERE invalid = 1
)
AND enabled = 1;
