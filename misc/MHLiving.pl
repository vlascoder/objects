#!/usr/bin/perl

use strict;
use utf8;

use Encode;
use File::Path qw(make_path);
use Getopt::Long;

use Data::Dumper;
use YAML;

use FindBin qw($RealBin);
use File::Basename;
use lib File::Basename::dirname($RealBin)."/modules";

use WebParser;
use Database;
use Object::RemoteContent;
use Object::Catalog;
use Object::Catalog::Section;
use Object::Catalog::Item;
use Object::Catalog::ItemToRemoteContent;
use Reference;

my (
    $UpdateSections,
    $ParseSections,
    $ParseItems,
);

GetOptions(
    'update_sections' => \$UpdateSections,
    'parse_sections' => \$ParseSections,
    'parse_items' => \$ParseItems,
);

if (!$UpdateSections && !$ParseSections && !$ParseItems)
{
    print qq{Usage:
    $0 [options]

Options:
    --update_sections
    --parse_sections
    --parse_items
};

    exit;
}


my $dbh = Database::InitDB(
    'ConfigFile' => "$RealBin/db.json"
)
or die "Cannot init DB";


my %CommonInitParams = (
    'dbh' => $dbh
);


my %Objects = (
    'RC'
        => Object::RemoteContent->new(%CommonInitParams),
    'Catalog'
        => Object::Catalog->new(%CommonInitParams),
    'Section'
        => Object::Catalog::Section->new(%CommonInitParams),
    'Item'
        => Object::Catalog::Item->new(%CommonInitParams),
    'ItemToRC'
        => Object::Catalog::ItemToRemoteContent->new(%CommonInitParams),
);

while (my ($ObjectName, $Class) = each %Objects)
{
    $Class->InitDBTable(
        'CreateIfNotExists' => 1
    );
}

my $References = Reference->new(
    %CommonInitParams
);

$References->AddTemplate(
    'Classes' => [
        'Catalog',
        'CatalogSection',
    ],
    'Description' => 'Catalog: Catalog <-> Sections',
)
or die "Cannot create reference template";

$References->AddTemplate(
    'Classes' => [
        'CatalogItem',
        'CatalogSection',
    ],
    'Description' => 'Catalog: Sections <-> Items',
)
or die "Cannot create reference template";

$References->AddTemplate(
    'Classes' => [
        'CatalogItem',
        'RemoteContent',
    ],
    'Token' => 'Gallery',
    'Description' => 'Catalog: Item gallery',
)
or die "Cannot create reference template";

my $WebParser = WebParser->new();

#=======================================
# Paste your code below
#=======================================

my $CatalogID = InitCatalog(
    'Name' => 'MHLiving'
) or die "Cannot init catalog";

if ($UpdateSections)
{
    ParseCatalogMenu(
        'CatalogID' => $CatalogID
    );
}

if ($ParseSections)
{
    my $Index = 0;

    while (
        my ($Section) = $Objects{'Section'}->Search(
            'CatalogID' => $CatalogID,
            'Offset' => $Index++,
            'Limit' => 1,
        )
    ) {
        ParseCatalogSection(
            'Section' => $Section,
        );
    }
}

if ($ParseItems)
{
    # Clean invalid RC and reparse related items {
    my $InvalidItems = $dbh->selectcol_arrayref(
        "SELECT i.id
        FROM catalog_items i
          JOIN references_links r1 ON r1.ref_tmpl_id = 3
            AND r1.from_id = i.id
          JOIN remote_content rc ON rc.id = r1.to_id
        WHERE rc.invalid = 1
        GROUP BY i.id"
    );

    if (scalar @$InvalidItems)
    {
        $dbh->do("UPDATE catalog_items SET parsed = NOW() - INTERVAL 7 DAY WHERE id IN (".join(',', grep { /^\d+$/ } @$InvalidItems).")");
    }

    while (1)
    {
        my $InvalidRC = $dbh->selectcol_arrayref(
            "SELECT id FROM remote_content WHERE invalid = 1 LIMIT 10"
        );

        last if not scalar @$InvalidRC;

        for my $ID (@$InvalidRC)
        {
            $dbh->do("DELETE FROM remote_content WHERE id = ?", undef, $ID);
            $dbh->do("DELETE FROM references_links WHERE ref_tmpl_id = 3 AND to_id = ?", undef, $ID);
        }
    }
    # }

    my $SectionIndex = 0;

    SECTION:
    while (
        my ($Section) = $Objects{'Section'}->Search(
            'CatalogID' => $CatalogID,
            'Enabled' => 1,
            'Offset' => $SectionIndex++,
            'Limit' => 1,
        )
    ) {
        my @ItemsLinks = $References->GetLinks(
            'Objects' => {
                'CatalogSection' => [$Section->{'ID'}],
                'CatalogItem' => [],
            },
        );

        ITEM:
        for my $Link (@ItemsLinks)
        {

            my ($Item) = $Objects{'Item'}->Search(
                'ID' => $Link->{'CatalogItem'}
            );

            if (!$Item)
            {
                warn "Invalid link: ".
                    "CatalogSection[". $Link->{'CatalogSection'} ."]".
                    " <-> ".
                    "CatalogItem[". $Link->{'CatalogItem'} ."]".
                    " ".
                    "(Item not found)";

                next ITEM;
            }

            next ITEM if
                   (0 && $Item->{'ParsedAge'} && $Item->{'ParsedAge'} < 1 * 24 * 60 * 60) # day
                || (0 && $Item->{'ParsedAge'} && $Item->{'ParsedAge'} < 7 * 24 * 60 * 60) # week
                || (1 && $Item->{'ParsedAge'} && $Item->{'ParsedAge'} < 30 * 24 * 60 * 60) # month
                || (0 && $Item->{'ParsedAge'} && $Item->{'ParsedAge'} < 30 * 60)          # half hour
                || (0 && $Item->{'ParsedAge'} && $Item->{'ParsedAge'} < 2 * 60 * 60)      # 2 hours
            ;

            ParseCatalogItem(
                'Item' => $Item,
            );

            sleep (1 + rand(3));
        }
    }
}


#=======================================

exit;

sub InitCatalog
{
    my (%Params) = @_;

    if ($Params{'Name'})
    {
        my @Catalogs = $Objects{'Catalog'}->Search(
            'Name' => $Params{'Name'}
        );

        if (scalar @Catalogs == 1)
        {
            return $Catalogs[0]->{'ID'};
        }
        elsif (not scalar @Catalogs)
        {
            return $Objects{'Catalog'}->Add(
                'Name' => $Params{'Name'}
            );
        }
    }
}

sub ParseCatalogMenu
{
    my (%Params) = @_;

    my $Result = $WebParser->HTTPRequest(
        'Method' => 'get',
        'URL' => 'http://mhliving.ru/',
    );

    if ($Result->{'Error'})
    {
        warn "Error: ". $Result->{'Error'};
        return;
    }

    $Result->{'Content'} =~ s/>\s+</></gs;
    $Result->{'Content'} =~ s/<!--.+?-->//gs;
    $Result->{'Content'} =~ s/<script.+?<\/script>//gs;

    my @Sections = ();

    while ($Result->{'Content'} =~ s/<a href="([^"]+)"[^>]*>(.+?)<\/a>//s)
    {
        my $URL = $1;
        my $Name = $2;

        if ($URL =~ /^\/catalog/)
        {
            $Name =~ s/<[^>]+>//gs;
            $Name = Encode::encode('utf8', $Name);

            push @Sections, {
                'Name' => $Name,
                'URL' => join('', 'http://mhliving.ru', $URL),
            };
        }
    }

    SECTION:
    for my $Section (@Sections)
    {
        my $IsParent = grep { $_->{'URL'} =~ /^$Section->{'URL'}./ } @Sections;
        next SECTION if $IsParent;

        my @ExistSections = $Objects{'Section'}->Search(
            'URL' => $Section->{'URL'},
        );

        if (not scalar @ExistSections)
        {
            my $Time = SQLTime();

            my $SectionID = $Objects{'Section'}->Add(
                'CatalogID' => $Params{'CatalogID'},
                'Name' => $Section->{'Name'},
                'URL' => $Section->{'URL'},
                'Created' => $Time,
                'Updated' => $Time,
                'Enabled' => 1,
            );

            $References->AddLinks(
                'Objects' => {
                    'Catalog' => [$Params{'CatalogID'}],
                    'CatalogSection' => [$SectionID],
                }
            );

            printf("New section added: id=$SectionID, name=%s, url=%s\n", $Section->{'Name'}, $Section->{'URL'});
        }
    }
}

sub ParseCatalogSection
{
    my (%Params) = @_;

    my $Page = 1;
    my $MaxPage = $Page;
    my $SearchMaxPage = 1;

    my $ItemRegExp = '<div class="itemBit"'.
        '.*?<span class=\'inlist_brand\'>([^<]+)</span>'.
        '.*?<a class="prodNameList" href="([^"]+?)"[^>]*>([^<]+)</a>'.
        '.*?<span.+?id="price">([^<]+)'.
        '.*?<div class="info">.*?<p>(.+?)</p>';

    while ($Page <= $MaxPage)
    {
        my $Result = $WebParser->HTTPRequest(
            'Method' => 'get',
            'URL' => $Params{'Section'}->{'URL'} . "?" . join('&',
                "PAGE_PROD_COUNT=48",
                "page=" . $Page++
            ),
        );

        if (!$Result->{'Error'})
        {
            if ($SearchMaxPage)
            {
                my ($Pagination) = $Result->{'Content'} =~ /<div class="pagination"[^>]*>(.+?)<\/div>/s;

                while ($Pagination =~ s/<a[^>]+>(\d+)<\/a>//s)
                {
                    $MaxPage = $1;
                }

                # print "max_page = $MaxPage\n";

                $SearchMaxPage = undef;
            }

            while ($Result->{'Content'} =~ s/$ItemRegExp//s)
            {
                my ($Brand, $URL, $Name, $Price, $Description) = ($1, $2, $3, $4, $5);

                $Brand =~ s/^\s+|\s+$//gs;

                $Price =~ s/\D+//gs;
                $Price .= ".00";

                $URL = join('', 'http://mhliving.ru', $URL);

                $Description =~ s/\s*<br[^>]*>\s*/\n/gs;
                $Description =~ s/^\s+|\s+$//gs;

                my %ItemData = (
                    'URL' => $URL,
                    'Name' => $Name,
                    'Brand' => $Brand,
                    'Price' => $Price,
                    'Description' => $Description,
                    'Enabled' => 1,
                );

                my %SearchParams = (
                    'URL' => $URL,
                );

                my @Items = $Objects{'Item'}->Search(
                    %SearchParams
                );

                my $Time = SQLTime();

                if (scalar(@Items) == 1)
                {
                    my @DifferentParams = ();

                    for my $ParamName (qw(Name Brand Price Description Enabled))
                    {
                        if ($ItemData{ $ParamName } ne $Items[0]->{ $ParamName })
                        {
                            push @DifferentParams, $ParamName;
                            # print $ParamName."=".$ItemData{ $ParamName }.",".$Items[0]->{ $ParamName }."\n";
                        }
                    }

                    if (scalar(@DifferentParams))
                    {
                        print "Item was changed (".join(', ',
                            "id=". $Items[0]->{'ID'},
                            "params='". join('|', @DifferentParams) ."'",
                            ). ")\n";

                        $Objects{'Item'}->Update(
                            %ItemData,
                            'ID' => $Items[0]->{'ID'},
                            'Updated' => $Time,
                        );
                    }

                    $References->AddLinks(
                        'Objects' => {
                            'CatalogItem' => [$Items[0]->{'ID'}],
                            'CatalogSection' => [$Params{'Section'}->{'ID'}],
                        }
                    );
                }
                elsif (not scalar(@Items))
                {
                    print "New item in section '". Encode::encode('utf8', $Params{'Section'}->{'Name'}) ."'\n";

                    my $ItemID = $Objects{'Item'}->Add(
                        %ItemData,
                        'Created' => $Time,
                        'Updated' => $Time,
                    );

                    if ($ItemID)
                    {
                        $References->AddLinks(
                            'Objects' => {
                                'CatalogItem' => [$ItemID],
                                'CatalogSection' => [$Params{'Section'}->{'ID'}],
                            }
                        );
                    }
                }
                else
                {
                    warn "Found similar items: ".join(', ', map { $_->{'ID'} } @Items);
                }
            }
        }

        sleep (1 + rand(6));
    }
}

sub ParseCatalogItem
{
    my (%Params) = @_;

    # print "Item: ". $Params{'Item'}->{'ID'} ."\n";

    my $Result = $WebParser->HTTPRequest(
        'Method' => 'get',
        'URL' => $Params{'Item'}->{'URL'},
    );

    if (!$Result->{'Error'})
    {
        my $HTML = $Result->{'Content'};
        $HTML =~ s/<(noscript|script|style).+?<\/\1>//gs;
        $HTML =~ s/<!--.+?-->/ /gs;
        $HTML =~ s/<(link|meta)[^>]+>//gs;
        $HTML =~ s/>\s+/>/gs;
        $HTML =~ s/\s+</</gs;

        my ($Artikul) = $HTML =~ /<div.+?class="article">.+?<span class="val">([^<]+)/;
        my ($LongDescription) = $HTML =~ /<section.+?itemprop="description">(.+?)<\/section>/;

        my (@Images) = $HTML =~ /<a\s+href="([^"]+)"\s+rel='gallery'>/gs;

        if (scalar @Images)
        {
            SaveCatalogItemRC(
                'Links' => \@Images,
                'ItemID' => $Params{'Item'}->{'ID'}
            );
        }

        my $Time = SQLTime();

        $Objects{'Item'}->Update(
            'ID' => $Params{'Item'}->{'ID'},
            'Artikul' => $Artikul,
            'LongDescription' => $LongDescription,
            'Parsed' => $Time,
            'Updated' => $Time,
        );

        if (0)
        {
            $HTML =~ s/>/>\n/gs;
            print "===\n$HTML\n===\n\n";
        }
        elsif (0)
        {
            print "$Artikul\n$LongDescription\n".join("\n", @Images)."\n";
        }
    }

    elsif ($Result->{'Code'} eq '404')
    {
        my $Time = SQLTime();

        $Objects{'Item'}->Update(
            'ID' => $Params{'Item'}->{'ID'},
            'Enabled' => 0,
            'Invalid' => 1,
            'Parsed' => $Time,
            'Updated' => $Time,
        );
    }

    else
    {
        warn $Result->{'Error'};
    }
}

#=======================================
#  System subs
#=======================================
sub SaveCatalogItemRC
{
    my (%Params) = @_;

    my @ItemRCLinks = $References->GetLinks(
        'Objects' => {
            'CatalogItem' => [$Params{'ItemID'}],
            'RemoteContent' => []
        },
        'Token' => 'Gallery',
    );

    my %EnabledRC = {};

    for my $Link (@{ $Params{'Links'} })
    {
        $Link =~ s/^\/\//http:\/\//s;
        $Link =~ s/^\//http:\/\/mhliving.ru\//s;

        my $Tail = '';

        if ($Link =~ s/\?(.*)$//s)
        {
            $Tail = $1;
        }

        my ($RC) = $Objects{'RC'}->Search(
            'URL' => $Link,
        );

        my $RemoteContentID = undef;

        if ($RC)
        {
            $RemoteContentID = $RC->{'ID'};
        }
        else
        {
            $RemoteContentID = $Objects{'RC'}->Add(
                'URL' => $Link,
                'Invalid' => 0,
            );
        }

        if ($RemoteContentID)
        {
            if (grep { $_->{'RemoteContent'} eq $RemoteContentID } @ItemRCLinks)
            {
                $EnabledRC{ $RemoteContentID } = undef;
            }
            else
            {
                $References->AddLinks(
                    'Objects' => {
                        'CatalogItem' => [$Params{'ItemID'}],
                        'RemoteContent' => [$RemoteContentID],
                    },
                    'Token' => 'Gallery',
                    'Enabled' => 1,
                );
            }
        }
    }

    # Enable/Disable link
    for my $Link (@ItemRCLinks)
    {
        if (
            $Link->{'Enabled'}
            && (not exists $EnabledRC{ $Link->{'RemoteContent'} })
        ){
            $References->UpdateLinks(
                'Objects' => {
                    'CatalogItem' => [ $Params{'ItemID'} ],
                    'RemoteContent' => [ $Link->{'RemoteContent'} ],
                },
                'Token' => 'Gallery',
                'Enabled' => 0,
            );
        }
        elsif (
            !$Link->{'Enabled'}
            && (exists $EnabledRC{ $Link->{'RemoteContent'} })
        ){
            $References->UpdateLinks(
                'Objects' => {
                    'CatalogItem' => [ $Params{'ItemID'} ],
                    'RemoteContent' => [ $Link->{'RemoteContent'} ],
                },
                'Token' => 'Gallery',
                'Enabled' => 1,
            );
        }
    }
}

sub SQLTime
{
    my $Time = sprintf(
        "%04d-%02d-%02d %02d:%02d:%02d",
        (localtime)[5] + 1900,
        (localtime)[4] + 1,
        (localtime)[3,2,1,0]
    );
}
