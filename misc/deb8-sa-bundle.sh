#!/bin/sh

BAKDIR=deb8sa.`date +%Y-%m-%d`.`date +%s`

mkdir ${BAKDIR}

mysqldump -u flowers flowers -p123 > ${BAKDIR}/db.sql

cp -R /home/sa/files ${BAKDIR}/files

tar -cjf ${BAKDIR}.tar.bz2 ${BAKDIR}

rm -rf ${BAKDIR}
