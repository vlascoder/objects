package Object::Stock;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {
        'Table' => 'stocks',
        'Priority' => 2000,
    };

    bless $Self, $Class;

    $Self->{'Fields'} = [
        {
            'Name' => 'Title',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'title',
            'HTML' => {
                'IsLink' => 1,
            }
        },
        {
            'Name' => 'Description',
            'Type' => 'Text',
            'DBFieldName' => 'descr',
        },
        {
            'Name' => 'Start',
            'Type' => 'DateTime',
            'DBFieldName' => 'start',
            'HTML' => {
                'Label' => 'Start time',
            }
        },
        {
            'Name' => 'Finish',
            'Type' => 'DateTime',
            'DBFieldName' => 'finish',
            'HTML' => {
                'Label' => 'Finish time',
            }
        },
        {
            'Name' => 'Enabled',
            'Type' => 'Boolean',
            'Default' => 1,
            'DBFieldName' => 'enabled',
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
