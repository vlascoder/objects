package Object::Order;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {
        'Table' => 'orders',
        'Priority' => 880,
    };

    bless $Self, $Class;

    $Self->{'Fields'} = [
        {
            'Name' => 'UserID',
            'Type' => 'Reference',
            'NotNull' => 1,
            'DBFieldName' => 'user_id',
            'HTML' => {
                'Label' => 'User',
            }
        },
        {
            'Name' => 'DeliveryAddressID',
            'Type' => 'Reference',
            'NotNull' => 1,
            'DBFieldName' => 'address_id',
            'HTML' => {
                'Label' => 'Address',
            }
        },
        {
            'Name' => 'Notes',
            'Type' => 'Text',
            'DBFieldName' => 'notes',
        },
        {
            'Name' => 'Price',
            'Type' => 'Decimal',
            'Default' => 0.0,
            'DBFieldName' => 'price',
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
