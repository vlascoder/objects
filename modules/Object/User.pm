package Object::User;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {
        'Table' => 'users',
        'Priority' => 899,
    };

    bless $Self, $Class;

    $Self->{'Fields'} = [
        {
            'Name' => 'FullName',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'fullname',
            'HTML' => {
                'Label' => 'Fullname',
                'IsLink' => 1,
            }
        },
        {
            'Name' => 'Phone',
            'Type' => 'String',
            'DBFieldName' => 'phone',
        },
        {
            'Name' => 'Email',
            'Type' => 'String',
            'DBFieldName' => 'email',
            'HTML' => {
                'Label' => 'E-mail',
            }
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
