package Object::Basket;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {
        'Table' => 'baskets',
        'Priority' => 870,
    };

    bless $Self, $Class;

    $Self->{'Fields'} = [
        {
            'Name' => 'UserID',
            'Type' => 'Reference',
            'NotNull' => 1,
            'DBFieldName' => 'user_id',
            'HTML' => {
                'Label' => 'User',
            }
        },
        {
            'Name' => 'Description',
            'Type' => 'Text',
            'DBFieldName' => 'descr',
            'HTML' => {
                'IsLink' => 1,
            }
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
