package Object::Bouquet;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {
        'Table' => 'bouquets',
        'Priority' => 1000
    };

    bless $Self, $Class;

    $Self->{'Table'} = 'bouquets';

    $Self->{'Fields'} = [
        {
            'Name' => 'Artikul',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'artikul',
            'HTML' => {
                'IsLink' => 1,
            }
        },
        {
            'Name' => 'Title',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'title',
            'HTML' => {
                'IsLink' => 1,
            }
        },
        {
            'Name' => 'Description',
            'Type' => 'Text',
            'DBFieldName' => 'descr',
        },
        {
            'Name' => 'Radius',
            'Type' => 'String',
            'DBFieldName' => 'size_r',
        },
        {
            'Name' => 'Height',
            'Type' => 'String',
            'DBFieldName' => 'size_h',
        },
        {
            'Name' => 'Price',
            'Type' => 'Decimal',
            'Default' => 0.0,
            'DBFieldName' => 'price',
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
