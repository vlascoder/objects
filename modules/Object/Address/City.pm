package Object::Address::City;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {};
    bless $Self, $Class;

    $Self->{'Table'} = 'address_cities';

    $Self->{'Fields'} = [
        {
            'Name' => 'Name',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'name',
            'HTML' => {
                'IsLink' => 1,
            }
        },
        {
            'Name' => 'Description',
            'Type' => 'Text',
            'DBFieldName' => 'descr',
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
