package Object::Address::CityRegion;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {};
    bless $Self, $Class;

    $Self->{'Table'} = 'address_city_regions';

    $Self->{'Fields'} = [
        {
            'Name' => 'CityID',
            'Type' => 'Reference',
            'NotNull' => 1,
            'DBFieldName' => 'city_id',
            'HTML' => {
                'Label' => 'City',
            }
        },
        {
            'Name' => 'Name',
            'Type' => 'String',
            'DBFieldName' => 'name',
            'HTML' => {
                'IsLink' => 1,
            }
        },
        {
            'Name' => 'DeliveryPrice',
            'Type' => 'Decimal',
            'Default' => 0.0,
            'DBFieldName' => 'delivery_price',
            'HTML' => {
                'Label' => 'Delivery price',
            }
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
