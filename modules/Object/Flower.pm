package Object::Flower;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {
        'Table' => 'flowers',
        'Priority' => 950
    };

    bless $Self, $Class;

    $Self->{'Fields'} = [
        {
            'Name' => 'Title',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'title',
            'HTML' => {
                'IsLink' => 1,
            }
        },
        {
            'Name' => 'Description',
            'Type' => 'Text',
            'DBFieldName' => 'descr',
        },
        {
            'Name' => 'Colors',
            'Type' => 'Text',
            'DBFieldName' => 'colors',
        },
        {
            'Name' => 'Price',
            'Type' => 'Decimal',
            'Default' => 0.0,
            'DBFieldName' => 'price',
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
