package Object::Feedback;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {
        'Table' => 'feedbacks',
        'Priority' => 860,
    };

    bless $Self, $Class;

    $Self->{'Fields'} = [
        {
            'Name' => 'UserID',
            'Type' => 'Reference',
            'NotNull' => 1,
            'DBFieldName' => 'user_id',
            'HTML' => {
                'Label' => 'User',
            }
        },
        {
            'Name' => 'Message',
            'Type' => 'Text',
            'DBFieldName' => 'descr',
            'HTML' => {
                'IsLink' => 1,
            }
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
