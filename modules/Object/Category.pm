package Object::Category;

use strict;

use base "Object";

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {
        'Table' => 'categories',
        'Priority' => 1100,
    };

    bless $Self, $Class;

    $Self->{'Fields'} = [
        {
            'Name' => 'Title',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'title',
            'HTML' => {
                'IsLink' => 1,
            }
        },
        {
            'Name' => 'Description',
            'Type' => 'Text',
            'DBFieldName' => 'descr',
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
