package Object::Catalog::Section;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {
        'Name' => 'Catalog section',
        'Token' => 'CatalogSection',
        'Table' => 'catalog_sections',
        'Priority' => 10190,
    };

    bless $Self, $Class;

    $Self->{'Fields'} = [
        {
            'Name' => 'CatalogID',
            'Type' => 'Reference',
            'NotNull' => 1,
            'DBFieldName' => 'catalog_id',
            'HTML' => {
                'Label' => 'Catalog',
                'Width' => 50,
            }
        },
        {
            'Name' => 'Name',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'name',
            'HTML' => {
                'Label' => 'Name',
                'IsLink' => 1,
                'Width' => 300,
            }
        },
        {
            'Name' => 'URL',
            'Type' => 'String',
            'DBFieldName' => 'url',
            'HTML' => {
                'Label' => 'URL',
            }
        },
        {
            'Name' => 'Created',
            'Type' => 'DateTime',
            'DBFieldName' => 'created',
            'HTML' => {
                'Label' => 'Created',
                'Width' => 90,
            }
        },
        {
            'Name' => 'Updated',
            'Type' => 'DateTime',
            'DBFieldName' => 'updated',
            'HTML' => {
                'Label' => 'Updated',
                'Width' => 90,
            }
        },
        {
            'Name' => 'Enabled',
            'Type' => 'Boolean',
            'DBFieldName' => 'enabled',
            'HTML' => {
                'Label' => 'Enabled',
                'Width' => 50,
            }
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
