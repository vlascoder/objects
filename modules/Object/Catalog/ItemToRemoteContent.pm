package Object::Catalog::ItemToRemoteContent;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {
        'Table' => 'catalog_item_to_rc',
        'Priority' => 10030,
    };

    bless $Self, $Class;

    $Self->{'Fields'} = [
        {
            'Name' => 'ItemID',
            'Type' => 'Reference',
            'NotNull' => 1,
            'DBFieldName' => 'item_id',
            'HTML' => {
                'Label' => 'Item',
            }
        },
        {
            'Name' => 'RemoteContentID',
            'Type' => 'Reference',
            'NotNull' => 1,
            'DBFieldName' => 'rc_id',
            'HTML' => {
                'Label' => 'Remote content',
            }
        },
        {
            'Name' => 'Enabled',
            'Type' => 'Boolean',
            'DBFieldName' => 'enabled',
            'HTML' => {
                'Label' => 'On',
            }
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
