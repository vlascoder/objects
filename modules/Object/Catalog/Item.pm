package Object::Catalog::Item;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {
        'Name' => 'Catalog item',
        'Token' => 'CatalogItem',
        'Table' => 'catalog_items',
        'Priority' => 10180,
    };

    bless $Self, $Class;

    $Self->{'Fields'} = [
        {
            'Name' => 'Artikul',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'artikul',
            'HTML' => {
                'Label' => 'Artikul',
                'IsLink' => 1,
                'Width' => 130,
            }
        },
        {
            'Name' => 'Name',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'name',
            'HTML' => {
                'Label' => 'Name',
                'IsLink' => 1,
            }
        },
        {
            'Name' => 'URL',
            'Type' => 'Text',
            'DBFieldName' => 'url',
            'HTML' => {
                'Label' => 'URL',
            }
        },
        {
            'Name' => 'Brand',
            'Type' => 'String',
            'DBFieldName' => 'brand',
            'HTML' => {
                'Label' => 'Brand',
                'IsHidden' => 0,
                'Width' => 130,
            }
        },
        {
            'Name' => 'Description',
            'Type' => 'Text',
            'DBFieldName' => 'descr',
            'HTML' => {
                'Label' => 'Description',
            }
        },
        {
            'Name' => 'LongDescription',
            'Type' => 'Text',
            'DBFieldName' => 'long_descr',
            'HTML' => {
                'Label' => 'Long description',
                'IsHidden' => 1,
            }
        },
        {
            'Name' => 'Stock',
            'Type' => 'Integer',
            'DBFieldName' => 'stock',
            'HTML' => {
                'Label' => 'Stock',
                'Width' => 60,
            }
        },
        {
            'Name' => 'Price',
            'Type' => 'Decimal',
            'DBFieldName' => 'price',
            'HTML' => {
                'Label' => 'Price',
                'Width' => 80,
            }
        },
        {
            'Name' => 'OldPrice',
            'Type' => 'Decimal',
            'DBFieldName' => 'old_price',
            'HTML' => {
                'Label' => 'OldPrice',
                'IsHidden' => 1,
            }
        },
        {
            'Name' => 'Created',
            'Type' => 'DateTime',
            'DBFieldName' => 'created',
            'HTML' => {
                'Label' => 'Created',
                'Width' => 90,
            }
        },
        {
            'Name' => 'Updated',
            'Type' => 'DateTime',
            'DBFieldName' => 'updated',
            'HTML' => {
                'Label' => 'Updated',
                'Width' => 90,
            }
        },
        {
            'Name' => 'Parsed',
            'Type' => 'DateTime',
            'DBFieldName' => 'parsed',
            'HTML' => {
                'Label' => 'Parsed',
                'Width' => 90,
            }
        },
        {
            'Name' => 'Enabled',
            'Type' => 'Boolean',
            'DBFieldName' => 'enabled',
            'HTML' => {
                'Label' => 'On',
                'Width' => 40,
            }
        },
        {
            'Name' => 'Invalid',
            'Type' => 'Boolean',
            'DBFieldName' => 'invalid',
            'HTML' => {
                'Label' => 'Invalid',
                'Width' => 40,
            }
        },
        {
            'Name' => 'SourceJSON',
            'Type' => 'Text',
            'DBFieldName' => 'source_json',
            'HTML' => {
                'IsHidden' => 1,
            }
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
