package Object::Address;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {};
    bless $Self, $Class;

    $Self->{'Table'} = 'addresses';

    $Self->{'Fields'} = [
        {
            'Name' => 'CityID',
            'Type' => 'Reference',
            'NotNull' => 1,
            'DBFieldName' => 'city_id',
            'HTML' => {
                'Label' => 'City',
            }
        },
        {
            'Name' => 'AsString',
            'Type' => 'String',
            'DBFieldName' => 'as_string',
            'HTML' => {
                'Label' => 'Address string',
                'IsLink' => 1,
            }
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
