package Object::Catalog;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {
        'Table' => 'catalogs',
        'Priority' => 10000,
    };

    bless $Self, $Class;

    $Self->{'Fields'} = [
        {
            'Name' => 'Name',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'name',
            'HTML' => {
                'Label' => 'Name',
                'IsLink' => 1,
            }
        },
        {
            'Name' => 'Comments',
            'Type' => 'String',
            'DBFieldName' => 'comments',
            'HTML' => {
                'Label' => 'Comments',
            }
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
