package Object::RemoteContent;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {
        'Table' => 'remote_content',
        'Priority' => 11000,
    };

    bless $Self, $Class;

    $Self->{'Fields'} = [
        {
            'Name' => 'URL',
            'Type' => 'Text',
            'NotNull' => 1,
            'DBFieldName' => 'url',
            'HTML' => {
                'Label' => 'URL',
                'IsLink' => 1,
            }
        },
        {
            'Name' => 'ContentType',
            'Type' => 'String',
            'DBFieldName' => 'content_type',
            'HTML' => {
                'Label' => 'Content type',
                'Width' => 150,
            }
        },
        {
            'Name' => 'ContentSize',
            'Type' => 'Integer',
            'DBFieldName' => 'content_size',
            'HTML' => {
                'Label' => 'Content size',
                'Width' => '120',
            }
        },
        {
            'Name' => 'Filename',
            'Type' => 'String',
            'DBFieldName' => 'filename',
            'HTML' => {
                'Label' => 'File name',
                'Width' => 200,
            }
        },
        {
            'Name' => 'Invalid',
            'Type' => 'Boolean',
            'DBFieldName' => 'invalid',
            'HTML' => {
                'Label' => 'Validity',
                'Width' => 80,
            }
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
