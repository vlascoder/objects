package Attribute;

use strict;

use base "Object";

use Attribute::String;

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {};
    bless $Self, $Class;

    $Self->{'Table'} = 'attributes';

    $Self->{'Fields'} = [
        {
            'Name' => 'ObjectType',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'obj_type',
        },
        {
            'Name' => 'ObjectID',
            'Type' => 'Integer',
            'NotNull' => 1,
            'DBFieldName' => 'object_id',
        },
        {
            'Name' => 'AttributeName',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'attribute_name',
        },
        {
            'Name' => 'AttributeType',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'attribute_type',
        },
        {
            'Name' => 'AttributeID',
            'Type' => 'Integer',
            'NotNull' => 1,
            'DBFieldName' => 'attribute_id',
        },
        {
            'Name' => 'Enabled',
            'Type' => 'Boolean',
            'Default' => 1,
            'DBFieldName' => 'enabled',
        }
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
