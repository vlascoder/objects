package Database;

use strict;
use utf8;

use DBI;
use JSON;
use Net::Domain;

use Exporter;
our @EXPORT = qw(init h);

my $pool;
my $MySQLConnectTimeout = 5;

sub init
{
    $pool = shift;
}

sub h
{
    my ($name)=@_;

    if( defined( $$pool{ $name } ) )
    {
        my $data=$$pool{ $name };

        if( not defined $$data{dbh} )
        {
            $$data{'dbh'}=DBI->connect(
                "DBI:mysql:".join(';',
                    "database=". $$data{'name'},
                    "host=". $$data{'host'},
                    "port=".($$data{'port'} || 3306),
                    "mysql_connect_timeout=". $MySQLConnectTimeout
                ),
                $$data{'user'},
                $$data{'pass'}
            );

            $$data{'dbh'}->do('SET NAMES utf8') if $$data{'dbh'};
            $$data{'dbh'}->{'mysql_enable_utf8'} = 1;
        }

        return $$data{dbh};
    }

    return undef;
}

sub InitDB
{
    my %Params = @_;

    if (!$Params{'ConfigFile'})
    {
        warn "Invalid ConfigFile";
        return;
    }
  
    if (! -e $Params{'ConfigFile'})
    {
        warn "ConfigFile not found";
        return;
    }

    open DBCONF, $Params{'ConfigFile'};

    my $DBConfig = JSON::decode_json(join('', <DBCONF>));

    if (!$DBConfig)
    {
        warn "Invalid db.json";
        return;
    }

    close DBCONF;

    my $Hostname = Net::Domain::hostfqdn();

    if (not exists $DBConfig->{ $Hostname })
    {
        warn "DB config not found";
        return;
    }

    init($DBConfig);

    return h($Hostname);
}

1;
