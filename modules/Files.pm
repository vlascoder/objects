package Files;

use strict;
use utf8;

sub new
{
  my ($Class, %Params) = @_;

  my $Self =
  {
    'CacheDir' => $Params{'CacheDir'},
  };

  bless $Self, $Class;

  return $Self;
}

sub UpdateImgFile
{
  my ($Self, $Type, $File, $URL) = @_;

  return undef if not exists $Self->{"Img${Type}Dir"};

  my $ImgDir = $Self->{"Img${Type}Dir"} || '.';

  $File = "${ImgDir}/${File}";
  my $TmpFile = "${File}.tmp";

  my $ModifyTime = 0;

  if (-e $File)
  {
    ($ModifyTime) = (stat $File)[9];
  }

  if ((!-e $File) || (-z $File) || ($ModifyTime lt (time - $Self->{'ImgUpdateTime'})))
  {
    my $NewData = $Self->HTTPGet($URL);

    open FILE, ">${TmpFile}";
    binmode FILE;
    print FILE $NewData;
    close FILE;

    if ((system("cmp -s $File $TmpFile") >> 8))
    {
      warn "File '$File' updated";
      `mv $TmpFile $File`;
    }
    else
    {
      unlink($TmpFile);
      `touch $File`;
    }
  }
}

sub StoreRemoteContent
{
  my ($Self, %Params) = @_;

  $Params{'ObjectType'} = ucfirst lc $Params{'ObjectType'};

  my $Filedir = $Self->{"Img". $Params{'ObjectType'} ."Dir"};

  if (!$Filedir || !-d $Filedir)
  {
    warn "Invalid Filedir";
    return;
  }

  my $Filename = $Params{'Filename'};

  if (!$Filename)
  {
    $Filename = $Params{'ObjectID'};

    if ($Params{'ObjectType'} eq 'Item')
    {
      if ($Params{'Type'} eq 'thumbnail')
      {
        $Filename .= '-thumb.jpg';
      }
      elsif ($Params{'Type'} eq 'gallery')
      {
        my $n = undef;

        for (my $i = 1; $i < 100; $i++)
        {
          next if -e "$Filedir/${Filename}_$i.jpg";
          $n = $i;
          last;
        }

        return undef if !$n;

        $Filename .= "_${n}.jpg";
      }
      elsif ($Params{'Type'} eq 'schema')
      {
        $Filename .= '_scheme';

        my $n = undef;

        for (my $i = 1; $i < 100; $i++)
        {
          next if -e "$Filedir/${Filename}_$i.jpg";
          $n = $i;
          last;
        }

        return undef if !$n;

        $Filename .= "_${n}.jpg";
      }

      else
      {
        $Filename = "rc" . $Params{'ID'};
      }
    }

    open FILE, ">$Filedir/$Filename" or die "Cannot open file for write: $Filedir/$Filename";
    binmode FILE;
    print FILE $Params{'Content'};
    close FILE;
  }

  return $Filename;
}

1;
