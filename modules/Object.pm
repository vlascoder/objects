package Object;

use strict;

sub Get
{
    my ($Self, %Params) = @_;

    if (!$Params{'ID'} || $Params{'ID'} !~ /^\d+$/)
    {
        warn "Invalid ID";
        return;
    }

    my @List = $Self->Search(
        'ID' => $Params{'ID'}
    );

    if (scalar(@List))
    {
        return %{ $List[0] };
    }
    else
    {
        return ();
    }
}

sub SearchCount
{
    my ($Self, %Params) = @_;

    return $Self->Search(
        %Params,
        'ReturnCount' => 1
    );
}

sub Search
{
    my ($Self, %Params) = @_;

    for my $ParamName (qw(Limit Offset))
    {
        if (defined $Params{ $ParamName })
        {
            $Params{ $ParamName } =~ s/\D+//gs;
        }
    }

    if (!$Params{'Limit'})
    {
        if (!$Params{'NoLimit'})
        {
            $Params{'Limit'} = 10;
        }
    }
    elsif ($Params{'Limit'} > 100)
    {
        $Params{'Limit'} = 100;
    }

    my @WhereFields = ();
    my @WhereValues = ();

    my @AddSelectFields = ();

    for my $Field (@{ $Self->{'Fields'} })
    {
        # Types
        if ($Field->{'Type'} eq 'DateTime')
        {
            push @AddSelectFields, 'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP('. $Field->{'DBFieldName'} .') AS ' . $Field->{'DBFieldName'} . "_age",
        }

        elsif ($Field->{'Type'} eq 'Integer')
        {
            if (exists $Params{ $Field->{'Name'}.'Min' })
            {
                push @WhereFields, $Field->{'DBFieldName'} . ' >= ?';
                push @WhereValues, $Params{ $Field->{'Name'}.'Min' };
            }

            if (exists $Params{ $Field->{'Name'}.'Max' })
            {
                push @WhereFields, $Field->{'DBFieldName'} . ' <= ?';
                push @WhereValues, $Params{ $Field->{'Name'}.'Max' };
            }
        }

        # NULL
        if (exists $Params{ $Field->{'Name'}.'IsNull' })
        {
            push @WhereFields, $Field->{'DBFieldName'} . ' IS NULL';
        }
        elsif (exists $Params{ $Field->{'Name'}.'IsNotNull' })
        {
            push @WhereFields, $Field->{'DBFieldName'} . ' IS NOT NULL';
        }

        # RegExp
        if (exists $Params{ $Field->{'Name'}.'RegExp' })
        {
            push @WhereFields, $Field->{'DBFieldName'} . ' REGEXP ?';
            push @WhereValues, $Params{ $Field->{'Name'}.'RegExp' };
        }

        if (exists $Params{ $Field->{'Name'} })
        {
            if (
                (ref $Params{ $Field->{'Name'} } eq 'ARRAY')
                && (scalar @{ $Params{ $Field->{'Name'} } })
            ){
                push @WhereFields, $Field->{'DBFieldName'}. " IN (". join(',', map {'?'} @{ $Params{ $Field->{'Name'} } }) .")";
                push @WhereValues, @{ $Params{ $Field->{'Name'} } };
            }

            elsif (not ref $Params{ $Field->{'Name'} })
            {
                push @WhereFields, $Field->{'DBFieldName'}. " = ?";
                push @WhereValues, $Params{ $Field->{'Name'} };
            }
        }
    }

    if ($Params{'ID'})
    {
        if (ref $Params{'ID'} eq 'ARRAY')
        {
            push @WhereFields, "id IN (". join(',', map {'?'} @{ $Params{'ID'} }) .")";
            push @WhereValues, @{ $Params{'ID'} };
        }

        elsif (not ref $Params{'ID'})
        {
            push @WhereFields, "id = ?";
            push @WhereValues, $Params{'ID'};
        }
    }

    my $SQL = "SELECT * FROM ". $Self->{'Table'} .
            (scalar(@WhereFields) ? " WHERE ". join(' AND ', @WhereFields) : '');


    if ($Params{'ReturnCount'})
    {
        $SQL =~ s/^SELECT \*/SELECT COUNT(*)/s;

        my ($Count) = $Self->{'dbh'}->selectrow_array(
            $SQL,
            undef,
            @WhereValues
        );

        return $Count;
    }
    elsif (scalar @AddSelectFields)
    {
        $SQL =~ s/^SELECT \*/"SELECT *, ". join(', ', @AddSelectFields)/se;
    }

    $SQL .= " ORDER BY id";

    $SQL .= (
        $Params{'Limit'}
            ? " LIMIT " . (
                $Params{'Offset'}
                ? $Params{'Offset'} . ', '
                : ''
            ). $Params{'Limit'}
            : ''
    );

    if ($Params{'Verbose'})
    {
        warn "---\n$SQL\n---\n".join(',', @WhereValues)."\n---\n";
    }

    my $ListRef = $Self->{'dbh'}->selectall_arrayref(
        $SQL,
        {'Slice' => {}},
        @WhereValues
    );

    my @List = ();

    for my $DBRow (@$ListRef)
    {
        my $Item = {
            'ID' => $DBRow->{'id'},
        };

=c
        while (my ($DBFieldName, $FieldName) = each %{ $Self->{'DBFieldsMap'} })
        {
            if (defined $DBRow->{ $DBFieldName })
            {
                $Item->{ $FieldName } = $DBRow->{ $DBFieldName };
            }
        }
=cut

        for my $Field (@{ $Self->{'Fields'} })
        {
            if (defined($DBRow->{ $Field->{'DBFieldName'} }))
            {
                $Item->{ $Field->{'Name'} } = $DBRow->{ $Field->{'DBFieldName'} };

                if ($Field->{'Type'} eq 'DateTime')
                {
                    $Item->{ join('', $Field->{'Name'}, 'Age') } = $DBRow->{ join('_', $Field->{'DBFieldName'}, 'age') };
                }
            }
        }

        push @List, $Item;
    }

    return @List;
}

sub Add
{
    my ($Self, %Params) = @_;

    my ($Set, $Values) = $Self->PrepareSet(%Params);

    if ($Set && $Values && scalar(@$Set))
    {
        $Self->{'dbh'}->do(
            "INSERT INTO ". $Self->{'Table'} ." (". join(',', @$Set) .") VALUES (". join(',', map {'?'} @$Set) .")",
            undef,
            @$Values
        );

        my $ID = $Self->{'dbh'}->last_insert_id(undef, undef, undef, undef);

        if ($ID)
        {
            return $ID;
        }

        else
        {
            warn "Cannot add item: ". join(', ', map { "$_ = '". $Params{ $_ } ."'" } sort keys %Params);
            warn $Self->{'dbh'}->errstr();
        }
    }

    else
    {
        warn "Nothing to add";
    }
}

sub Update
{
    my ($Self, %Params) = @_;

    if (!$Params{'ID'} || $Params{'ID'} !~ /^\d+$/)
    {
        warn "Invalid ID";
        return;
    }

    my ($Set, $Values) = $Self->PrepareSet(%Params);

    if ($Set && $Values && scalar(@$Set))
    {
        my $Updated = $Self->{'dbh'}->do(
            "UPDATE ". $Self->{'Table'} ." SET ". join(', ', map { "$_ = ?" } @$Set) ." WHERE id = ?",
            undef,
            @$Values, $Params{'ID'}
        );

        if (!$Updated)
        {
            warn "Cannot update item: ". join(', ', map { "$_ = '". $Params{ $_ } ."'" } sort keys %Params);
            warn $Self->{'dbh'}->errstr();
        }
    }

    else
    {
        warn "Nothing to update";
    }

    return 1;
}

sub Delete
{
    my ($Self, %Params) = @_;

    if (!$Params{'ID'} || $Params{'ID'} !~ /^\d+$/)
    {
        warn "Invalid ID";
        return;
    }

    my $Deleted = $Self->{'dbh'}->do(
        "DELETE FROM ". $Self->{'Table'} ." WHERE id = ?",
        undef,
        $Params{'ID'}
    );

    return $Deleted;
}

sub PrepareSet
{
    my ($Self, %Params) = @_;

    my @Set = ();
    my @Values = ();

    for my $Field (@{ $Self->{'Fields'} })
    {
        my $Value = $Params{ $Field->{'Name'} };

        next if not defined $Value;

        push @Set, $Field->{'DBFieldName'};
        push @Values, $Value;
    }

    return \@Set, \@Values;
}

sub Init
{
    my ($Self, %Params) = @_;

    if (!$Params{'dbh'})
    {
        warn "Invalid dbh";
        return;
    }

    $Self->{'dbh'} = $Params{'dbh'};

    if ($Self->{'Fields'} && (ref($Self->{'Fields'}) eq 'ARRAY'))
    {
        for my $Field (@{ $Self->{'Fields'} })
        {
            $Self->{'FieldsHash'}->{ $Field->{'Name'} } = $Field;
            $Self->{'DBFieldsMap'}->{ $Field->{'DBFieldName'} } = $Field->{'Name'};
        }
    }

    $Self->InitDBTable();

    return 1;
}

sub InitDBTable
{
    my ($Self, %Params) = @_;

    my $Tables = $Self->{'dbh'}->selectcol_arrayref(
        "SHOW TABLES LIKE ?",
        undef,
        $Self->{'Table'}
    );

    if ($Tables && scalar(@$Tables))
    {
        return 1;
    }

    warn "Table ". $Self->{'Table'} ." not found\n";

=c
    my $r = $Self->{'dbh'}->selectall_arrayref(
        "DESCRIBE ". $Self->{'Table'},
        {'Slice' => {}}
    );

    if ($r && scalar(@$r))
    {
        # warn "Table '". $Self->{'Table'} ."' already exists\n";
        return;
    }
=cut

    my @Fields = ();
    my @Errors = ();
    my $Tab = (' ' x 4);

    if ($Self->{'Fields'} && (ref($Self->{'Fields'}) eq 'ARRAY'))
    {
        for my $Field (@{ $Self->{'Fields'} })
        {
            my $Definition = "${Tab}". $Field->{'DBFieldName'};

            if ($Field->{'Type'} eq 'String')
            {
                $Definition .= " VARCHAR(255)";
            }

            elsif ($Field->{'Type'} eq 'Text')
            {
                $Definition .= " TEXT";
            }

            elsif ($Field->{'Type'} eq 'Reference')
            {
                $Definition .= " INT UNSIGNED";
            }

            elsif ($Field->{'Type'} eq 'Integer')
            {
                $Definition .= " INT UNSIGNED";
            }

            elsif ($Field->{'Type'} eq 'Decimal')
            {
                $Definition .= " DECIMAL(10,2)";
            }

            elsif ($Field->{'Type'} eq 'DateTime')
            {
                $Definition .= " DATETIME";
            }

            elsif ($Field->{'Type'} eq 'Boolean')
            {
                $Definition .= " SMALLINT(1)";
            }

            else
            {
                push @Errors, "Unknown type '". $Field->{'Type'} ."' for field '". $Field->{'Name'} ."' in table '". $Self->{'Table'} ."'";
                $Definition = undef;
            }

            if ($Definition)
            {
                if ($Field->{'NotNull'})
                {
                    $Definition .= ' NOT NULL';
                }

                if ($Field->{'Default'})
                {
                    $Definition .= ' DEFAULT '. $Field->{'Default'};
                }

                push @Fields, $Definition;
            }
        }
    }

    if (scalar @Errors)
    {
        warn join("\n", @Errors) ."\n";
        return;
    }

    else
    {
        my $SQL = "CREATE TABLE ". $Self->{'Table'} ." (\n".
            "${Tab}id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,\n".
            join(",\n", @Fields)."\n".
        ")\n".
        "ENGINE = innodb\n".
        "CHARACTER SET = utf8\n".
        ";";

        print "======\n$SQL\n======\n\n";

        if ($Params{'CreateIfNotExists'})
        {
            my $Result = $Self->{'dbh'}->do($SQL);
            if ($Result)
            {
                warn "Table ". $Self->{'Table'} ." successfully created";
                return 1;
            }
            else
            {
                warn "Error while creating table ". $Self->{'Table'};
                return;
            }
        }
    }
}

sub Fields
{
    my ($Self, %Params) = @_;

    return @{ $Self->{'Fields'} };
}

sub Priority
{
    my ($Self) = @_;

    return $Self->{'Priority'} || 0;
}

sub Name
{
    my ($Self) = @_;

    return $Self->{'Name'} || '';
}

sub Token
{
    my ($Self) = @_;

    return $Self->{'Token'} || '';
}

1;
