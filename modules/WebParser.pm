package WebParser;

use strict;
use utf8;

use LWP;

my %ResponseHeaders = (
  'Content-Type' => 1,
  'Content-Length' => 1,
  'ETag' => 1,
);

sub new
{
  my ($Class, %Params) = @_;

  my $Self =
  {
    'CacheDir'        => $Params{'CacheDir'},
    'CacheLifeTime'   => ($Params{'CacheLifeTime'} || 1*24*60*60), # 1 day
  };

  bless $Self, $Class;

  $Self->{'ua'} = $Self->UserAgent(
    'UserAgentStr' => $Params{'UserAgentStr'},
    'Proxy' => $Params{'Proxy'}
  );

  return $Self;
}

sub UserAgent
{
  my ($Self, %Params) = @_;

  my $UserAgentStr = $Params{'UserAgentStr'}
    || 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.1 Safari/537.37';

  my $ua = LWP::UserAgent->new(
    'agent' => $UserAgentStr
  );

  if ($Params{'Proxy'})
  {
    $ua->proxy('http' => $Params{'Proxy'});
  }

  return $ua;
}

sub HTTPGet
{
  my ($Self, $URL, %Params) = @_;

  my $HTTPResponse = $Self->{'ua'}->get($URL);

  my $html = undef;

  if ($HTTPResponse->is_success)
  {
    $html = $HTTPResponse->decoded_content;
  }
  else
  {
    warn "Error: " . $HTTPResponse->status_line . ", URL='$URL'";
  }

  return $html;
}

sub HTTPRequest
{
  my ($Self, %Params) = @_;

  my $Content = undef;
  my %Headers = ();
  my $Error = undef;
  my $Code = undef;

  if (!$Params{'Method'} || $Params{'Method'} !~ /^(?:get|head)$/i)
  {
    $Error = 'Invalid Method';
  }
  else
  {
    my $HTTPResponse;

    if ($Params{'Verbose'})
    {
      warn "HTTPRequest: ".$Params{'URL'};
    }

    if (lc($Params{'Method'}) eq 'get')
    {
      $HTTPResponse = $Self->{'ua'}->get($Params{'URL'});
    }
    elsif (lc($Params{'Method'}) eq 'head')
    {
      $HTTPResponse = $Self->{'ua'}->head($Params{'URL'});
    }

    $Code = $HTTPResponse->code();

    if ($HTTPResponse->is_success)
    {
      if (lc($Params{'Method'}) eq 'get')
      {
        $Content = $HTTPResponse->decoded_content;
      }

      if ($Params{'Headers'} || lc($Params{'Method'}) eq 'head')
      {
        while (my ($HeaderName, $Enabled) = each %ResponseHeaders)
        {
          $Headers{ $HeaderName } = $HTTPResponse->header($HeaderName)
            if $Enabled;
        }
      }
    }
    else
    {
      $Error = $HTTPResponse->status_line;
      warn "HTTPRequest error: $Error, URL: ". $Params{'URL'};
    }
  }

  return {
    'Content' => $Content,
    'Headers' => \%Headers,
    'Error' => $Error,
    'Code' => $Code,
  };
}

sub GetHTML
{
  my ($Self, $type, $id, $url) = @_;

  return undef if (!$type && !$id) || !$url;

  my $html;

  my ($CacheFile, $CacheFileModTime);

  if ($Self->{'CacheDir'} && -e $Self->{'CacheDir'})
  {
    $CacheFile = $Self->{'CacheDir'} . "/$type-$id";
  }

  $CacheFileModTime = (stat($CacheFile))[9] if $CacheFile;

  if (-e $CacheFile && ($CacheFileModTime gt (time - $Self->{'CacheLifeTime'})))
  {
    warn "Cache: $url";

    open HTMLCACHE, $CacheFile;
    $html = join '', <HTMLCACHE>;
    close HTMLCACHE;

    $html = decode('utf8', $html);
  }
  else
  {
    # warn "HTTPGet: $url";

    $html = $Self->HTTPGet($url);
    $html =~ s/>[\r\n\s]+</></gs;

    # if ($html && -e $CacheFile)
    if ($html && -d $Self->{'CacheDir'})
    {
      open HTML_CACHE_RW, ">$CacheFile";
      binmode HTML_CACHE_RW, ':utf8';
      print HTML_CACHE_RW $html;
      close HTML_CACHE_RW;
    }
  }

  return $html;
}

sub ParseULTree
{
  my ($Self, $HTMLRef) = @_;

    $$HTMLRef =~ s/<!--.+?-->//gs;
    $$HTMLRef =~ s/style=["'][^"']*["']//gs;

    my %Lists = ();

    my $ListIndex = 1;

    my %Items = ();
    my %ItemsLinks = ();

    my $ItemID = 1;

    while (1)
    {
      my $End = index $$HTMLRef, '</ul>';
      last if $End < 0;

      my $Start = rindex $$HTMLRef, '<ul>', $End;
      last if $Start < 0;

      my $Body = substr $$HTMLRef, $Start, $End + 5 - $Start, "[% UL${ListIndex} %]";
      $Body =~ s/\s+/ /gs;
      $Body =~ s/>\s+|\s+<//gs;

      my @Items = $Body =~ /<li>(.+?)<\/li>/gs;

      for my $Item (@Items)
      {
        # Get child lists
        my @ChildLists = ();

        while ($Item =~ s/\[\%\s*UL(\d+)\s*\%\]//s)
        {
          push @ChildLists, $1;
        }

        # Identify item
        if ($Item =~ /href/s)
        {
          my ($URL) = $Item =~ /href=["']([^"']+)["'][^>]*>([^<]+)/s;

          my $Text = $Item;
          $Text =~ s/<[^>]+>//gs;

          $Text =~ s/^\s+|\s+$//gs;

          $Item = {
            'ID' => $ItemID++,
            'Title' => $Text,
            'URL' => $URL
          };
        }
        else
        {
          $Item = {
            'ID' => $ItemID++,
            'Title' => $Item
          };
        }

        $Items{ $Item->{'ID'} } = $Item;

        for my $ChildListID (@ChildLists)
        {
          for my $ChildItem (@{ $Lists{ $ChildListID }->{'Items'} })
          {
            $Items{ $ChildItem->{'ID'} }->{'ParentID'} = $Item->{'ID'};

            $ItemsLinks{ $Item->{'ID'} } ||= [];
            push @{ $ItemsLinks{ $Item->{'ID'} } }, $ChildItem->{'ID'};
          }
        }
      }

      $Lists{ $ListIndex++ } = {
        'Items' => \@Items
      };
    }

    while (my ($ItemID, $ItemData) = each %Items)
    {
      if (not exists $ItemData->{'ParentID'})
      {
        $ItemsLinks{'ROOT'} ||= [];
        push @{ $ItemsLinks{'ROOT'} }, $ItemID;
      }
    }

    return {
      'Items' => \%Items,
      'Links' => \%ItemsLinks
    };
}

1;
