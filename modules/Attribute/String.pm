package Attribute::String;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {};
    bless $Self, $Class;

    $Self->{'Table'} = 'attribute_values_string';

    $Self->{'Fields'} = [
        {
            'Name' => 'Value',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'attr_value',
        },
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
