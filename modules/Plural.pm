package Plural;

use strict;
use utf8;

my %Plurals = (
    'Address' => 'Addresses',
    'AddressCity' => 'AddressCities',
    'AddressCityRegion' => 'AddressCityRegions',
    'Basket' => 'Baskets',
    'Bouquet' => 'Bouquets',
    'Category' => 'Categories',
    'Feedback' => 'Feedbacks',
    'Flower' => 'Flowers',
    'Order' => 'Orders',
    'Stock' => 'Stocks',
    'User' => 'Users',
);

sub Do
{
    my (%Params) = @_;

    return
        (exists $Plurals{ $Params{'String'} })
        ? $Plurals{ $Params{'String'} }
        : $Params{'String'};
}

1;
