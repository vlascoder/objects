package Reference;

use strict;

use Reference::Template;
use Reference::Links;

sub new
{
    my ($Class, %Params) = @_;

    if (!$Params{'dbh'})
    {
        warn "No dbh";
        return;
    }

    my $Self = {};

    bless $Self, $Class;

    my %CommonInitParams = (
        'dbh' => $Params{'dbh'},
    );

    $Self->{'Template'}    = Reference::Template->new(%CommonInitParams);
    $Self->{'Links'}       = Reference::Links->new(%CommonInitParams);

    for my $ObjClass (qw(Template Links))
    {
        $Self->{ $ObjClass }->InitDBTable(
            'CreateIfNotExists' => 1
        );
    }

    return $Self;
}

=item AddTemplate
    AddTemplate(
        'Classes' => [
            'CatalogItem',
            'CatalogSection',
        ],
        'Token' => '',
        'Description' => '',
        'MaxLinks' => undef,
        'Enabled' => 1
    );
=cut

sub AddTemplate
{
    my ($Self, %Params) = @_;

    return if !$Self->CheckTemplateParams(%Params);

    my ($Template) = $Self->GetTemplate(%Params);

    if ($Template)
    {
        return $Template->{'ID'};
    }
        
    my @Classes = sort { $a cmp $b } map { s/^\s+|\s+$//gs; $_ } @{ $Params{'Classes'} };

    $Params{'Token'} ||= 'Default';

    if (not defined $Params{'Enabled'})
    {
        $Params{'Enabled'} = 1;
    }

    my $ID = $Self->{'Template'}->Add(
        'FromObjectClass' => $Classes[0],
        'ToObjectClass' => $Classes[1],
        'Token' => $Params{'Token'},
        'Description' => $Params{'Description'},
        'MaxLinks' => $Params{'MaxLinks'},
        'Enabled' => $Params{'Enabled'},
    );

    if (!$ID)
    {
        warn "Cannot create reference template";
    }

    return $ID;
}

=item GetTemplate
    GetTemplate(
        'Classes' => [
            'CatalogItem',
            'CatalogSection',
        ],
        'Token' => '',
        'Enabled' => 1
    );
=cut

sub GetTemplate
{
    my ($Self, %Params) = @_;

    return if not $Self->CheckTemplateParams(%Params);

    my @Classes = sort { $a cmp $b } map { s/^\s+|\s+$//gs; $_ } @{ $Params{'Classes'} };

    $Params{'Token'} ||= 'Default';

    my ($Template) = $Self->{'Template'}->Search(
        'FromObjectClass' => $Classes[0],
        'ToObjectClass' => $Classes[1],
        'Token' => $Params{'Token'},
    );

    return $Template;
}

sub GetClassTemplates
{
    my ($Self, %Params) = @_;

    my @Templates = ();

    for my $Direction (qw(From To))
    {
        push @Templates, (
            $Self->{'Template'}->Search(
                $Direction.'ObjectClass' => $Params{'Class'},
                'Enabled' => 1,
            )
        );
    }

    if ($Params{'CountLinks'} && $Params{'ObjectID'})
    {
        for my $Template (@Templates)
        {
            my %SearchParams = (
                'Objects' => {},
                'Token' => $Template->{'Token'},
                'Enabled' => (defined $Params{'Enabled'} ? $Params{'Enabled'} : undef)
            );

            for my $Direction (qw(From To))
            {
                my @IDs = ();

                if ($Template->{ $Direction.'ObjectClass' } eq $Params{'Class'})
                {
                    push @IDs, $Params{'ObjectID'};
                }

                $SearchParams{'Objects'}->{ $Template->{ $Direction.'ObjectClass' } } = [ @IDs ];
            }

            $Template->{'Count'} = $Self->GetLinksCount(%SearchParams);
        }
    }

    return @Templates;
}

=item CheckTemplateParams
    CheckTemplateParams(
        'Classes' => [
            'CatalogItem',
            'CatalogSection'
        ]
    );
=cut

sub CheckTemplateParams
{
    my ($Self, %Params) = @_;

    my %Classes = ();

    for my $Class (@{ $Params{'Classes'} })
    {
        $Class =~ s/^\s+|\s+$//gs;
        $Classes{ $Class } = undef;
    }

    return (scalar keys %Classes) == 2 ? 1 : undef;
}

sub DeleteTemplate
{
    my ($Self, %Params) = @_;

    if (!$Params{'ID'})
    {
        warn "Invalid TemplateID";
        return;
    }

    my $LinksCount = $Self->{'Links'}->SearchCount(
        'TemplateID' => $Params{'ID'}
    );

    warn "c=$LinksCount";

    if ($LinksCount)
    {
        warn "Cannot delete template: found $LinksCount references";
        return;
    }

    return $Self->{'Template'}->Delete(
        'ID' => $Params{'ID'}
    );
}

sub GetLinksCount
{
    my ($Self, %Params) = @_;

    return $Self->GetLinks(
        %Params,
        'CountUp' => 1
    );
}

=item GetLinks
    Links(
        'Objects' => {
            'CatalogItem' => [1,2,3],
            'CatalogSection' => []
        },
        'Token' => 'Gallery'
    );
=cut

sub GetLinks
{
    my ($Self, %Params) = @_;

    return if !$Params{'Objects'}
        || (ref($Params{'Objects'}) ne 'HASH');

    my %Objects = ();

    while (my ($Class, $IDs) = each %{ $Params{'Objects'} })
    {
        $Class =~ s/^\s+|\s+$//gs;

        $Objects{ $Class } = ($IDs || []);
    }

    my @Classes = sort { $a cmp $b } keys %Objects;

    if (!$Self->CheckTemplateParams('Classes' => [@Classes]))
    {
        warn "Invalid classes";
        return;
    }

    my $Template = $Self->GetTemplate(
        'Classes' => [@Classes],
        'Token' => $Params{'Token'}
    );

    if (!$Template)
    {
        warn "Template not found";
        return;
    }

    my %SearchParams = (
        'TemplateID' => $Template->{'ID'},
        'FromID' => $Objects{ $Classes[0] },
        'ToID' => $Objects{ $Classes[1] },
    );

    if (defined $Params{'Enabled'})
    {
        $SearchParams{'Enabled'} = $Params{'Enabled'};
    }

    if ($Params{'CountUp'})
    {
        return $Self->{'Links'}->SearchCount(%SearchParams);
    }

    my @Links = $Self->{'Links'}->Search(
        %SearchParams,
        'NoLimit' => 1
    );

    return map {
        {
            $Classes[0] => $_->{'FromID'},
            $Classes[1] => $_->{'ToID'},
            'Enabled' => $_->{'Enabled'}
        }
    } @Links;
}

=item AddLinks
    AddLinks(
        'Objects' => {
            'CatalogItem' => [1,2,3],
            'CatalogSection' => [5,4]
        },
        'Token' => 'Gallery'
    );
=cut

sub AddLinks
{
    my ($Self, %Params) = @_;

    require YAML;

    # warn "AddLinks: ". YAML::Dump(\%Params);

    if (!$Params{'Objects'})
    {
        warn "AddLinks: No objects";
        return;
    }

    if (ref($Params{'Objects'}) ne 'HASH')
    {
        warn "AddLinks: Invalid objects (". ref($Params{'Objects'}) .")";
        return;
    }

    my %Objects = ();

    my $MaxLinks = 0;

    while (my ($Class, $IDs) = each %{ $Params{'Objects'} })
    {
        $Class =~ s/^\s+|\s+$//gs;

        if (!$IDs || ref($IDs) ne 'ARRAY')
        {
            warn "AddLinks: Invalid IDs for $Class";
            return;
        }

        $Objects{ $Class } = $IDs;

        $MaxLinks = scalar(@{ $IDs }) if scalar(@{ $IDs }) > $MaxLinks;
    }

    # warn "AddLinks: Current max_links: $MaxLinks";

    my @Classes = sort { $a cmp $b } keys %Objects;

    if (!$Self->CheckTemplateParams('Classes' => [@Classes]))
    {
        warn "AddLinks: Invalid classes";
        return;
    }

    my $Template = $Self->GetTemplate(
        'Classes' => [@Classes],
        'Token' => $Params{'Token'}
    );

    if (!$Template)
    {
        warn "AddLinks: Template not found";
        return;
    }

    # warn "AddLinks: TemplateID: ".$Template->{'ID'};

    if (
        $Template->{'MaxLinks'}
        && ($MaxLinks > $Template->{'MaxLinks'})
    ) {
        warn "AddLinks: Too many links (". $MaxLinks ." of ". $Template->{'MaxLinks'} .")";
        return;
    }

    if (not defined $Params{'Enabled'})
    {
        $Params{'Enabled'} = 1;
    }

    my $Count = 0;

    for my $FromID (@{ $Objects{ $Classes[0] } })
    {
        for my $ToID (@{ $Objects{ $Classes[1] } })
        {
            my ($Link) = $Self->GetLinks(
                'Objects' => {
                    $Classes[0] => [$FromID],
                    $Classes[1] => [$ToID],
                },
                'Token' => $Params{'Token'},
                # 'TemplateID' => $Template->{'ID'},
                # 'FromID' => $FromID,
                # 'ToID' => $ToID,
            );

            # warn "AddLinks: Link ". YAML::Dump($Link);

            if (!$Link)
            {
                warn "Add link ". $Classes[0] ."[". $FromID ."] -> ". $Classes[1] ."[". $ToID ."]";

                my $ID = $Self->{'Links'}->Add(
                    'TemplateID' => $Template->{'ID'},
                    'FromID' => $FromID,
                    'ToID' => $ToID,
                    'Enabled' => $Params{'Enabled'},
                );

                if (!$ID)
                {
                    warn "AddLinks: Cannot add link ". $Classes[0] ."[". $FromID ."] -> ". $Classes[1] ."[". $ToID ."]";
                }
                else
                {
                    $Count++;
                }
            }
            elsif ($Link->{'Enabled'} ne $Params{'Enabled'})
            {
                warn "Update link ". $Classes[0] ."[". $FromID ."] -> ". $Classes[1] ."[". $ToID ."]";

                $Self->{'Links'}->Update(
                    'ID' => $Link->{'ID'},
                    'Enabled' => $Params{'Enabled'},
                );
            }
        }
    }
}

=item UpdateLinks
    UpdateLinks(
        'Objects' => {
            'CatalogItem' => [1,2,3],
            'CatalogSection' => [5,4]
        },
        'Token' => 'Gallery',
        'Ebanled' => 1,
    );
=cut

sub UpdateLinks
{
    my ($Self, %Params) = @_;

    if (!$Params{'Objects'})
    {
        warn "No objects";
        return;
    }

    if (ref($Params{'Objects'}) ne 'HASH')
    {
        warn "Invalid objects (". ref($Params{'Objects'}) .")";
        return;
    }

    if (not defined $Params{'Enabled'})
    {
        warn "Invalid Enabled param";
        return;
    }

    my %Objects = ();

    while (my ($Class, $IDs) = each %{ $Params{'Objects'} })
    {
        $Class =~ s/^\s+|\s+$//gs;

        if (!$IDs || (ref($IDs) ne 'ARRAY') || !scalar(@{ $IDs }))
        {
            warn "No IDs for $Class";
            return;
        }

        $Objects{ $Class } = $IDs;
    }

    my @Classes = sort { $a cmp $b } keys %Objects;

    if (!$Self->CheckTemplateParams('Classes' => [@Classes]))
    {
        warn "Invalid classes";
        return;
    }

    my $Template = $Self->GetTemplate(
        'Classes' => [@Classes],
        'Token' => $Params{'Token'}
    );

    if (!$Template)
    {
        warn "Template not found";
        return;
    }

    my $Count = 0;

    for my $FromID (@{ $Objects{ $Classes[0] } })
    {
        for my $ToID (@{ $Objects{ $Classes[1] } })
        {
            my ($Link) = $Self->{'Links'}->Search(
                'TemplateID' => $Template->{'ID'},
                'FromID' => $FromID,
                'ToID' => $ToID,
            );

            if (!$Link)
            {
                warn "Link not found: ". $Classes[0] ."[". $FromID ."] -> ". $Classes[1] ."[". $ToID ."]";
            }

            else
            {
                warn "Update link: ". $Classes[0] ."[". $FromID ."] -> ". $Classes[1] ."[". $ToID ."]";

                my $Updated = $Self->{'Links'}->Update(
                    'ID' => $Link->{'ID'},
                    'Enabled' => $Params{'Enabled'},
                );

                if (!$Updated)
                {
                    warn "Cannot update link: ". $Classes[0] ."[". $FromID ."] -> ". $Classes[1] ."[". $ToID ."]";
                }
                else
                {
                    $Count++;
                }
            }
        }
    }

    return $Count;
}

1;
