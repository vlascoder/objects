package WebParser::LoftArt;

use strict;
use Encode;
use utf8;

use base 'WebParser';

sub GetAllCatalogSections
{
    my ($Self, %Params) = @_;

    my $BaseURL = 'http://loft-art.ru';

    my $Result = $Self->HTTPRequest(
        'Method' => 'get',
        'URL' => $BaseURL
    );

    if ($Result->{'Error'})
    {
        warn "Error: ". $Result->{'Error'};
        return;
    }

    $Result->{'Content'} =~ s/>\s+</></gs;
    $Result->{'Content'} =~ s/<!--.+?-->//gs;
    $Result->{'Content'} =~ s/<script.+?<\/script>//gs;

    my @Sections = ();

    LI:
    while ($Result->{'Content'} =~ s/<li([^>]+)><a([^>]+)>(.+?)<\/a>//s)
    {
        my $Li_Attrs = $1;
        my $A_Attrs = $2;
        my $A_Text = $3;

        next LI if $Li_Attrs !~ /nav-item/;

        my ($SectionURL) = grep { /^href=/ } split(/\s+/, $A_Attrs);

        if ($SectionURL)
        {
            $SectionURL =~ s/^href=["']|["']$//gs;

            if ($SectionURL ne '#')
            {
                $A_Text =~ s/<[^>]+>//gs;
                $A_Text =~ s/&[^;]+;/ /gs;
                $A_Text =~ s/\s+/ /gs;
                $A_Text =~ s/^\s+|\s+$//gs;
                $A_Text = Encode::encode('utf8', $A_Text);
                $A_Text =~ s/(?:New|Hot)!$//gs;

                push @Sections, {
                    'Name' => $A_Text,
                    'URL' => $SectionURL,
                };
            }
        }
    }

    return @Sections;
}

sub GetAllSectionItems
{
    my ($Self, %Params) = @_;

    warn $Params{'URL'};

    my @Items = ();

    my $Result = $Self->HTTPRequest(
        'Method' => 'get',
        'URL' => $Params{'URL'} . "?" . join('&',
            "limit=all",
        ),
    );

    if (!$Result->{'Error'})
    {
        my @LIs = $Result->{'Content'} =~ /<li class="item"[^>]*>(.+?)<\/li>/gs;

        for my $LI (@LIs)
        {
            my %Item = (
                'Enabled' => 1
            );

            ($Item{'URL'}, $Item{'Name'}) = $LI =~ /<h2 class="product-name">.*?<a href="([^"]+?)"[^>]*>([^<]+)/s;
            ($Item{'Price'}) = $LI =~ /<span class="regular-price".*?<span class="price">([^<]+)/s;
            ($Item{'Artikul'}) = $LI =~ /<div class="sku">.+?<\/span>(.*?)<\/div>/s;
            ($Item{'Dimensions'}) = $LI =~ /<div class="size">.+?<\/span>(.*?)<\/div>/s;

            if (!$Item{'Price'})
            {
                ($Item{'Price'}) = $LI =~ /<p class="special-price".*?<span class="price"[^>]+>([^<]+)/s;
            }

            for my $Key (qw(Name Artikul))
            {
                $Item{ $Key } =~ s/^\s+|\s+$//gs;
                $Item{ $Key } =~ s/\s+/ /gs;
            }

            for my $Key (qw(Price))
            {
                $Item{ $Key } =~ s/\D+//gs if $Item{ $Key };
                $Item{ $Key } ||= 0;
                $Item{ $Key } .= '.00';
            }

            $Item{'OldPrice'} = $Item{'Price'};
            $Item{'Price'} = sprintf("%0.2f", $Item{'Price'} * 1.1);

            push @Items, \%Item;
        }
    }

    return @Items;
}

sub GetItemData
{
    my ($Self, %Params) = @_;

    warn $Params{'URL'};

    my $Result = $Self->HTTPRequest(
        'Method' => 'get',
        'URL' => $Params{'URL'},
    );

    my %ItemData = ();

    if (!$Result->{'Error'})
    {
        my $HTML = $Result->{'Content'};

        $HTML =~ s/<(noscript|script|style).+?<\/\1>//gs;
        $HTML =~ s/<!--.+?-->/ /gs;
        $HTML =~ s/<(link|meta)[^>]+>//gs;
        $HTML =~ s/>\s+/>/gs;
        $HTML =~ s/\s+</</gs;

        my ($Description) = $HTML =~ /<div class="std" itemprop="description">(.+?)<\/div>/s;

        my @DescriptionRows = ();

        $Description =~ s/<[^>]+>/\n/gs;
        $Description =~ s/^\s*(?:Выбрать\s+(?:расцветку\s+ткани|цвет\s+ножек)|МОЖНО\s+ЗДЕСЬ)\s*$//gm;
        $Description =~ s/^(?:.+\-|\S+)$//gm;
        $Description =~ s/\n+/\n/gs;
        $Description =~ s/^\s+|\s+$//gs;

        $ItemData{'Description'} = $Description;

        my @Images = $HTML =~ /<a href="([^"]+)[^>]+cloud-zoom-gallery/gs;

        $ItemData{'Images'} = \@Images;
    }

    elsif ($Result->{'Code'} eq '404')
    {
        $ItemData{'Crawler::NotFound'} = 1;
    }

    return %ItemData;
}

1;
