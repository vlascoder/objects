package Admin;

use strict;

# use Exporter;
# our @EXPORT = qw(Add List);

# Core modules
use Database;

use Reference;

use Attribute;

# Models
use Object::Address;
use Object::Address::City;
use Object::Address::CityRegion;
use Object::Basket;
use Object::Bouquet;
use Object::Category;
use Object::Feedback;
use Object::Flower;
use Object::Order;
use Object::Packaging;
use Object::Stock;
use Object::User;

use Object::RemoteContent;
use Object::Catalog;
use Object::Catalog::Section;
use Object::Catalog::Item;

sub new
{
    my ($Class, %Params) = @_;

    if (!$Params{'DBAccount'})
    {
        warn "No DB account";
        return;
    }

    my $Self = {};

    bless $Self, $Class;

    Database::init($Params{'DBAccount'});
    my $dbh = Database::h('main');

    if (!$dbh)
    {
        warn "Invalid DB account 'main'";
        return;
    }

    my %CommonInitParams = (
        'dbh' => $dbh,
    );

    $Self->{'Objects'} = {};

    $Self->{'Objects'}->{'Address'}             = Object::Address->new(%CommonInitParams);
    $Self->{'Objects'}->{'AddressCity'}         = Object::Address::City->new(%CommonInitParams);
    $Self->{'Objects'}->{'AddressCityRegion'}   = Object::Address::CityRegion->new(%CommonInitParams);
    $Self->{'Objects'}->{'Basket'}              = Object::Basket->new(%CommonInitParams);
    $Self->{'Objects'}->{'Bouquet'}             = Object::Bouquet->new(%CommonInitParams);
    $Self->{'Objects'}->{'Category'}            = Object::Category->new(%CommonInitParams);
    $Self->{'Objects'}->{'Feedback'}            = Object::Feedback->new(%CommonInitParams);
    $Self->{'Objects'}->{'Flower'}              = Object::Flower->new(%CommonInitParams);
    $Self->{'Objects'}->{'Order'}               = Object::Order->new(%CommonInitParams);
    $Self->{'Objects'}->{'Packaging'}           = Object::Packaging->new(%CommonInitParams);
    $Self->{'Objects'}->{'Stock'}               = Object::Stock->new(%CommonInitParams);
    $Self->{'Objects'}->{'User'}                = Object::User->new(%CommonInitParams);

    $Self->{'Objects'}->{'RemoteContent'}       = Object::RemoteContent->new(%CommonInitParams);
    $Self->{'Objects'}->{'Catalog'}             = Object::Catalog->new(%CommonInitParams);
    $Self->{'Objects'}->{'CatalogSection'}      = Object::Catalog::Section->new(%CommonInitParams);
    $Self->{'Objects'}->{'CatalogItem'}         = Object::Catalog::Item->new(%CommonInitParams);

    $Self->{'Reference'}                        = Reference->new(%CommonInitParams);

    return $Self;
}

sub Fields
{
    my ($Self, $Type, %Params) = @_;

    return if not $Self->CheckObject($Type);

    return $Self->{'Objects'}->{ $Type }->Fields(%Params);
}

sub Count
{
    my ($Self, $Type, %Params) = @_;

    return if not $Self->CheckObject($Type);

    return $Self->{'Objects'}->{ $Type }->SearchCount(%Params);
}

sub List
{
    my ($Self, $Type, %Params) = @_;

    return if not $Self->CheckObject($Type);

=c
    # Check or add default search params
        'Order' => [qw(ID)],
        'Offset' => 0,
        'Limit' => 10
=cut

    return $Self->{'Objects'}->{ $Type }->Search(%Params);
}

sub Add
{
    my ($Self, $Type, %Params) = @_;

    return if not $Self->CheckObject($Type);

    return $Self->{'Objects'}->{ $Type }->Add(%Params);
}

sub Update
{
    my ($Self, $Type, %Params) = @_;

    return if not $Self->CheckObject($Type);

    return $Self->{'Objects'}->{ $Type }->Update(%Params);
}

sub Delete
{
    my ($Self, $Type, %Params) = @_;

    return if not $Self->CheckObject($Type);

    return $Self->{'Objects'}->{ $Type }->Delete(%Params);
}

sub Get
{
    my ($Self, $Type, %Params) = @_;

    return if not $Self->CheckObject($Type);

    return $Self->{'Objects'}->{ $Type }->Get(%Params);
}

sub Search
{
    my ($Self, $Type, %Params) = @_;

    return if not $Self->CheckObject($Type);

    return $Self->{'Objects'}->{ $Type }->Search(%Params);
}

sub CheckObject
{
    my ($Self, $Type) = @_;

    return exists $Self->{'Objects'}->{ $Type };
}

sub GetObjects
{
    my ($Self, $Type) = @_;

    my @Objects = sort
    {
        ($Self->{'Objects'}->{ $b }->Priority() <=> $Self->{'Objects'}->{ $a }->Priority())
        || ($a cmp $b)
    }
    keys %{ $Self->{'Objects'} };

    return @Objects;
}

sub InitDBTable
{
    my ($Self, $Type) = @_;

    return if not $Self->CheckObject($Type);

    return $Self->{'Objects'}->{ $Type }->InitDBTable();
}

sub GetRefTemplates
{
    my ($Self, %Params) = @_;

    return $Self->{'Reference'}->GetClassTemplates(
        'Class' => $Params{'Class'},
        'ObjectID' => $Params{'ObjectID'},
        'Enabled' => $Params{'Enabled'},
        'CountLinks' => $Params{'CountLinks'},
    );
}

1;
