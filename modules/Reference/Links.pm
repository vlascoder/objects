package Reference::Links;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {};
    bless $Self, $Class;

    $Self->{'Table'} = 'references_links';

    $Self->{'Fields'} = [
        {
            'Name' => 'TemplateID',
            'Type' => 'Integer',
            'NotNull' => 1,
            'DBFieldName' => 'ref_tmpl_id',
        },
        {
            'Name' => 'FromID',
            'Type' => 'Integer',
            'NotNull' => 1,
            'DBFieldName' => 'from_id',
        },
        {
            'Name' => 'ToID',
            'Type' => 'Integer',
            'NotNull' => 1,
            'DBFieldName' => 'to_id',
        },
        {
            'Name' => 'Enabled',
            'Type' => 'Boolean',
            'Default' => 1,
            'DBFieldName' => 'enabled',
        }
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
