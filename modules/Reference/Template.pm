package Reference::Template;

use strict;

use base "Object";

=item

=cut

sub new
{
    my ($Class, %Params) = @_;

    my $Self = {};
    bless $Self, $Class;

    $Self->{'Table'} = 'references_templates';

    $Self->{'Fields'} = [
        {
            'Name' => 'FromObjectClass',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'from_obj_class',
        },
        {
            'Name' => 'ToObjectClass',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'to_obj_class',
        },
        {
            'Name' => 'Token',
            'Type' => 'String',
            'NotNull' => 1,
            'DBFieldName' => 'token',
        },
        {
            'Name' => 'Description',
            'Type' => 'String',
            'DBFieldName' => 'descr',
        },
        {
            'Name' => 'MaxLinks',
            'Type' => 'Integer',
            'DBFieldName' => 'max_links',
        },
        {
            'Name' => 'Enabled',
            'Type' => 'Boolean',
            'Default' => 1,
            'DBFieldName' => 'enabled',
        }
    ];

    return if not $Self->Init(%Params);

    return $Self;
}

1;
