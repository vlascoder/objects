package Translate;

use strict;
use utf8;

my %Strings = (
    'ru' => {
        'Add' => 'Добавить',
        'Address' => 'Адрес',
        'Address string' => 'Адрес',
        'AddressCities' => 'Города',
        'AddressCityRegions' => 'Районы',
        'Addresses' => 'Адреса',
        'Artikul' => 'Артикул',

        'Baskets' => 'Корзины',
        'Bouquet' => 'Букет',
        'Bouquets' => 'Букеты',
        'Brand' => 'Бренд',

        'Categories' => 'Категории',
        'City' => 'Город',
        'Colors' => 'Цвета',
        'Created' => 'Добавлено',

        'Delete' => 'Удалить',
        'Delivery price' => 'Стоимость доставки',
        'Description' => 'Описание',

        'Empty' => 'Пусто',
        'Enabled' => 'Действительный',

        'Feedbacks' => 'Отзывы',
        'Flower' => 'Цветок',
        'Flowers' => 'Цветы',
        'Fullname' => 'Имя',
        'Height' => 'Высота',
        'Message' => 'Сообщение',
        'Name' => 'Название',
        'Notes' => 'Заметки',
        'Orders' => 'Заказы',
        'Packaging' => 'Упаковка',
        'Parsed' => 'Импортировано',
        'Phone' => 'Телефон',
        'Price' => 'Цена',
        'Radius' => 'Радиус',
        'Save' => 'Сохранить',
        'Stock' => 'Склад',
        'Stocks' => 'Акции',
        'Title' => 'Название',
        'Updated' => 'Обновлено',
        'User' => 'Пользователь',
        'Users' => 'Пользователи',

        'Item successfully updated' => 'Обновлено успешно',
        'Item successfully added' => 'Добавлено успешно',
        'No data' => 'Нет данных',
        'Start time' => 'Время начала',
        'Finish time' => 'Время окончания',
    }
);

sub Do
{
    my (%Params) = @_;

    return
        (exists $Strings{ $Params{'Language'} }->{ $Params{'String'} })
        ? $Strings{ $Params{'Language'} }->{ $Params{'String'} }
        : $Params{'String'};
}

1;
